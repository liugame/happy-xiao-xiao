/**
 * 我的奖励弹窗
 */
cc.Class({
    extends: cc.Component,

    properties: {
        scrollView: {
            default: null,
            type: cc.ScrollView
        },
        item: {
            default: null,
            type: cc.Prefab
        },
        spacing: 0
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        cc.vv.netRoot.on("goToMall", () => {
            this.node.removeFromParent();
        });
    },
    onDisable() {
        cc.vv.netRoot.off("goToMall");
    },

    start() {
        
    },

    init: function (data) {
        if (!data || !data.length) {
            return;
        }
        this.scrollView.content.height = data.length * (this.item.data.height + this.spacing) - this.spacing;
        for (let i = 0; i < data.length; i++) {
            let item = cc.instantiate(this.item);
            this.scrollView.content.addChild(item);
            item.setPosition(0, -item.height * i - this.spacing * (i + 1));
            item.getComponent("ItemReward").init(data[i]);
        }
    },

    //关闭页面
    onCloseBtnClick: function () {
        cc.vv.MusicComment.playSFX("click");
        this.node.removeFromParent();
    },
});
