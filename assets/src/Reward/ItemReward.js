/**
 * 单项我的奖励
 */
var DataMgr = require('DataMgr');
cc.Class({
    extends: cc.Component,

    properties: {
        img: {
            default: null,
            type: cc.Node
        },
        btn: {
            default: null,
            type: cc.Node
        },
        labTime: {
            default: null,
            type: cc.Label
        },
        btnSprteFrames: {
            type: cc.SpriteFrame,
            default: []
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {

    },

    init: function (data) {
        this.data = data;

        const spriteCom = this.img.getComponent(cc.Sprite);
        cc.vv.UIComment.loadOriginImg(data.img, spriteCom);

        /**
         * card_type 1anta优惠券 2微信优惠券 3卡密
         * status 1未领取，2已领取
         */

        /**按钮 [可领取，复制卡密，去使用（已领取）] */
        this.labTime.string = "有效期：" + data.valid_time;
        let spIdx = 0;
        if (data.card_type == 3) {      //复制cdkey
            spIdx = 1;
        } else if (data.status == 1) {  //可领取
            spIdx = 0;
        } else {    //去使用
            spIdx = 2;
        }
        this.spIdx = spIdx;
        this.btn.getComponent(cc.Sprite).spriteFrame = this.btnSprteFrames[spIdx];
    },
    onClick: function () {
        cc.vv.MusicComment.playSFX("click");

        if (this.spIdx == 0) {      //未领取
            cc.vv.JJSdk.getGiftCard(this.data.user_card_id, res => {
                if (typeof res.data != "string") {
                    cc.vv.WXComment.wxGameGotoMain(res.data && res.data.path);
                } else {
                    cc.vv.netRootjs.dispatchEvent("netError", "领取成功");
                    this.data.status = 2;
                    this.spIdx = 2;
                    this.btn.getComponent(cc.Sprite).spriteFrame = this.btnSprteFrames[2];
                }
            })
        } else if (this.spIdx == 1) {   //我的奖励弹窗查看卡密
            cc.vv.JJSdk.setClipboardData(this.data.cdkey);
        } else if (this.spIdx == 2) {
            cc.vv.WXComment.wxGameGotoMain(this.data.path);
        }
    }
});
