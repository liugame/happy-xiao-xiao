/**
 * 每日任务单项
 */
cc.Class({
    extends: cc.Component,

    properties: {
        iconImg: {
            type: cc.Sprite,
            default: null
        },
        progressImg: {
            type: cc.Sprite,
            default: null
        },
        titleLab: {
            type: cc.Label,
            default: null
        },
        rewardLab: {
            type: cc.Label,
            default: null
        },
        progressLab: {
            type: cc.Label,
            default: null
        },
        btn: {
            default: null,
            type: cc.Node
        },
        btnSprteFrames: {
            type: cc.SpriteFrame,
            default: []
        }
    },

    start() {
    },

    init: function (data) {
        const spriteCom = this.iconImg.getComponent(cc.Sprite),
            atlas = 'plist/invitaion',
            icon = data.icon,
            task_id = data.task_id;
        let type = 1;
        cc.vv.UIComment.loadSpriteFrame("icon" + icon, spriteCom, atlas);
        this.btn.getComponent(cc.Button).enableAutoGrayEffect = true;

        const canRecv = data.curr >= data.denominator;  //只管任务进度，不管是否已领取  
        //btnSprteFrames = ["可领取","邀请","得体力","得炸弹","得锤子","已领取"]
        if (data.is_get) {
            type = 6;
        } else {
            if (canRecv) {      //如果进度满足，都显示可领取
                type = 1;
            } else if (task_id == 1) {     //每日签到
                type = 1;
                this.btn.getComponent(cc.Button).interactable = false;
            } else if (task_id == 2) {      //邀请
                type = 2;
            } else if (task_id == 3) {      //分享得体力
                type = 3;
            } else if (task_id == 4) {      //得炸弹
                type = 4;
            } else if (task_id == 5) {      //得锤子
                type = 5;
            } else if (task_id == 6) {      //晚上8点签到
                let date = new Date();
                let h = date.getHours(), m = date.getMinutes();
                if (h >= 20 && h < 21 && m <= 30) {
                    data.curr = data.denominator;
                } else {
                    this.btn.getComponent(cc.Button).interactable = false;
                }
                type = 1;
            }
        }

        this.titleLab.string = data.name;
        this.rewardLab.string = "x" + data.reward;
        data.curr > data.denominator && (data.curr = data.denominator);
        this.progressLab.string = data.curr + "/" + data.denominator;
        this.progressImg.fillRange = data.curr / data.denominator;

        this.btn.getComponent(cc.Sprite).spriteFrame = this.btnSprteFrames[type - 1];
        this.btn.getComponent(cc.Button).enabled = type != 6;    //已领取

        this.type = type;
        this.data = data;
    },

    onClick: function () {
        cc.vv.MusicComment.playSFX("click");

        const type = this.type, task_id = this.data.task_id;
        if (type == 1) {
            if (task_id == 6) {
                cc.vv.JJSdk.signIn(task_id, res => {
                    cc.vv.JJSdk.getTaskGift(task_id, res => {
                        this.data.type = 6;
                        this.data.curr = this.data.denominator;
                        this.btn.getComponent(cc.Button).enabled = false;
                        this.btn.getComponent(cc.Sprite).spriteFrame = this.btnSprteFrames[this.data.type - 1];
                        Globals.hp++;
                        cc.vv.netRootjs.dispatchEvent("addHp");
                    });
                })
            } else {
                cc.vv.JJSdk.getTaskGift(task_id, res => {
                    if (task_id == 1) {
                        this.data.type = 6;
                        this.data.curr = this.data.denominator;
                        this.btn.getComponent(cc.Button).enabled = false;
                        Globals.hp++;
                        cc.vv.netRootjs.dispatchEvent("addHp");
                    } else if (task_id == 2 || task_id == 3) {
                        Globals.hp++;
                        cc.vv.netRootjs.dispatchEvent("addHp");
                    } else if (task_id == 4) {
                        Globals.boomNum++;
                        cc.vv.netRootjs.dispatchEvent("addBoomNum");
                    } else if (task_id == 5) {
                        Globals.malletNum++;
                        cc.vv.netRootjs.dispatchEvent("addMalletNum");
                    }
                    this.data.type = 6;
                    this.data.curr = this.data.denominator;
                    this.btn.getComponent(cc.Sprite).spriteFrame = this.btnSprteFrames[5];      //已领取
                    this.btn.getComponent(cc.Button).enable = false;
                    cc.vv.netRootjs.dispatchEvent("netError", "领取成功");
                })
            }
        } else if (type == 2) {
            cc.vv.WXComment.wxShare(4);
        } else if (type == 3) {
            cc.vv.WXComment.wxShare(1);
        } else if (type == 4) {
            cc.vv.WXComment.wxShare(3);
        } else if (type == 5) {
            cc.vv.WXComment.wxShare(2);
        }
    },
});
