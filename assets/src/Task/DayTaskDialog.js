/**
 * 每日任务弹窗
 */
cc.Class({
    extends: cc.Component,

    properties: {
        scrollView: {
            default: null,
            type: cc.ScrollView
        },
        item: {
            default: null,
            type: cc.Prefab
        },
        spacing: 0
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {

    },

    start() {

    },

    init: function (data) {
        if (!data || !data.length) {
            this.scrollView.active = false;
            return;
        }
        this.scrollView.active = true;

        const content = this.scrollView.content, children = content.children;
        this.scrollView.height = content.height = data.length * (this.item.data.height + this.spacing) - this.spacing;
        for (let i = 0; i < data.length; i++) {
            children[i].getComponent("ItemTask").init(data[i]);
        }
    },

    //关闭页面
    onCloseBtnClick: function () {
        cc.vv.MusicComment.playSFX("click");
        this.node.removeFromParent();
    },
});
