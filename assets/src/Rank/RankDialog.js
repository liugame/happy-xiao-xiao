/**
 * 排行榜弹窗
 */
var DataMgr = require('DataMgr');
cc.Class({
    extends: cc.Component,

    properties: {
        scrollView: {
            default: null,
            type: cc.ScrollView
        },
        tabCurrOn: {
            type: cc.Node,
            default: null
        },
        tabCurrOff: {
            type: cc.Node,
            default: null
        },
        tabLastOn: {
            type: cc.Node,
            default: null
        },
        tabLastOff: {
            type: cc.Node,
            default: null
        },
        item: {
            type: cc.Prefab,
            default: null
        },
        imgHead: {
            default: null,
            type: cc.Node
        },
        labRanking: {
            default: null,
            type: cc.Label
        },
        labNickname: {
            default: null,
            type: cc.Label
        },
        labScore: {
            default: null,
            type: cc.Label
        },
        myRank: {
            default: null,
            type: cc.Node
        },
        btnRecv: {
            default: null,
            type: cc.Node
        },
        imgNoReward: {
            default: null,
            type: cc.Node
        },
        btnSprteFrames: {
            type: cc.SpriteFrame,
            default: []
        },
        labNobody: {
            default: null,
            type: cc.Label
        },
        spacing: 0
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.model = null;
    },

    init: function (idx) {
        if (idx == 0) {
            this.onTabCurrClick();
        } else {
            this.onTabLastClick();
        }
    },
    initPage: function (data) {
        const me = data.me, other = data.others;
        if (!me || !me.index) {
            this.myRank.active = false;
        } else {
            this.myRank.active = true;

            const spriteCom = this.imgHead.getComponent(cc.Sprite);
            cc.vv.UIComment.loadOriginImg(me.avatarUrl + "?aa=aa.jpg", spriteCom);
            this.labRanking.string = me.index;
            this.labNickname.string = cc.vv.LocalData.formatNickname(me.nickName);
            this.labScore.string = me.score;

            /**
             * 1.可领取，未领取；上周有奖励，只显示可领取按钮
             * 2.已领取，上周有奖励，只显示已领取
             * 3.未上榜，上周未上榜，不显示分数，不显示领取按钮，显示未上榜图
             * 4.没有排名，如本周，就只显示分数
             */
            let type = 4;
            if (this.model == "last") {
                this.lastIndex = me.index;
                if (me.is_success == 1) {   //上榜
                    type = me.is_get == 1 ? 2 : 1;
                } else {
                    type = 3;
                }
            }
            this.btnRecv.active = type == 1 || type == 2;
            this.btnRecv.getComponent(cc.Sprite).spriteFrame = this.btnSprteFrames[type - 1];
            this.imgNoReward.active = type == 3;
            this.labScore.node.active = type == 4;
        }

        if (other && other.length) {
            this.labNobody.node.active = false;
            this.scrollView.content.height = other.length * (this.item.data.height + this.spacing) - this.spacing;
            for (let i = 0; i < other.length; i++) {
                let item = cc.instantiate(this.item);
                this.scrollView.content.addChild(item);
                item.setPosition(0, -item.height * i - this.spacing * (i + 1) - (1 - item.anchorY) * item.height);
                item.getComponent("ItemRank").init(other[i]);
            }
        } else {
            this.labNobody.node.active = true;
        }
    },
    onRecvClick: function () {
        cc.vv.MusicComment.playSFX("click");
        //领取排行榜奖励
        cc.vv.JJSdk.getWeekRankCard(res => {
            res.data.index = this.lastIndex;
            cc.vv.netRootjs.netRootjs("weekRankReward", res);
            this.btnRecv.getComponent(cc.Sprite).spriteFrame = this.btnSprteFrames[1];
            this.btnRecv.getComponent(cc.Button).enabled = false;
        })
    },
    onTabCurrClick: function () {
        if (this.model == "curr") {
            return;
        }
        this.model = "curr";
        this.tabCurrOn.active = this.tabLastOff.active = true;
        this.tabCurrOff.active = this.tabLastOn.active = false;
        cc.vv.JJSdk.weekrank(res => {
            this.scrollView.content.removeAllChildren();
            this.initPage(res.data);
        });
    },
    onTabLastClick: function () {
        if (this.model == "last") {
            return;
        }
        this.model = "last";
        this.tabCurrOn.active = this.tabLastOff.active = false;
        this.tabCurrOff.active = this.tabLastOn.active = true;
        cc.vv.JJSdk.lastweekrank(res => {
            this.scrollView.content.removeAllChildren();
            this.initPage(res.data);
        });
    },
    //关闭页面
    onCloseBtnClick: function () {
        cc.vv.MusicComment.playSFX("click");
        this.node.removeFromParent();
    },
});
