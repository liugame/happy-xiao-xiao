/**
 * 单项排行榜
 */
var DataMgr = require('DataMgr');
cc.Class({
    extends: cc.Component,

    properties: {
        imgHead: {
            default: null,
            type: cc.Node
        },
        labRanking: {
            default: null,
            type: cc.Label
        },
        labNickname: {
            default: null,
            type: cc.Label
        },
        labScore: {
            default: null,
            type: cc.Label
        },
        imgBg: {
            default: null,
            type: cc.Node
        },
        spBg: {     //纯绿色背景
            default: null,
            type: cc.Node,
            tooltip: "纯绿色背景"
        },
        bgSpriteFrames: {
            default: [],
            type: cc.SpriteFrame
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {

    },

    init: function (data) {
        const spriteCom = this.imgHead.getComponent(cc.Sprite);
        cc.vv.UIComment.loadOriginImg(data.avatarUrl + "?aa=aa.jpg", spriteCom);
        this.labRanking.string = data.index;
        this.labNickname.string = cc.vv.LocalData.formatNickname(data.nickname);
        this.labScore.string = data.score;

        if (data.index < 4) {
            this.imgBg.active = true;
            this.spBg.active = false;
            this.imgBg.getComponent(cc.Sprite).spriteFrame = this.bgSpriteFrames[data.index - 1];
        } else {
            this.imgBg.active = false;
            this.spBg.active = true;
        }
    }
});
