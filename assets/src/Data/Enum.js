/**
 * 枚举
 */
var State = cc.Enum({
    STATE_NOEN: "noen",
    STATE_CLICK: "click",//点击
    STATE_LINE: "line",//行
    STATE_COLUMN: "column",//列
    STATE_CROSS: "cross",//十字
    STATE_SIMILAR: "similar",//同类
    STATE_ANCHOR: "anchor",//MAO
    STATE_BIRD: "bird",
});
var Enum = {
    MoveTime: 0.3,
    EliminateTime: 0.4,
    RepairTime: 0.5,
    NONE: "none",
    UP: 8,
    DOWN: 2,
    LEFT: 4,
    RIGHT: 6,
    elementScore: 1,
    three: 3,
    four: 4,
    five: 5,
    random: 100,
    doorSize: 30,
    infiniteScore: 200,     //挑战模式小关超过该分数就要增加关卡（难度）
};
Enum.State = State;
window.Enum = Enum;