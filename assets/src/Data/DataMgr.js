/**
 * 元素及关卡配置
 */
var DataMgr = {};


//格子配置
var latticeDts = [
    {
        //透明格子
        id: "0",
        img: "",
        thinIce: {       //冰
            img: ["", "", ""],
            hp: 0
        },
        shield: {        //盾
            img: ["", "", ""],
            hp: 0,
            eliminate: true,//true是不可消除，false是可消除
        },
        elementID: 0,
        elementHp: 0,
        show: false,
        born: false,
    },
    {
        //普通格子
        id: "1",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //出生点
        id: "3",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: true,
        },
        elementID: 0,
        elementHp: 0,
        show: false,
        born: true,
    },
    {
        //薄冰1
        id: "C011",
        img: "",
        thinIce: {
            img: ["thinice_1"],
            hp: 1
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //薄冰2
        id: "C012",
        img: "",
        thinIce: {
            img: ["thinice_1", "thinice_2"],
            hp: 2
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //薄冰3
        id: "C013",
        img: "",
        thinIce: {
            img: ["thinice_1", "thinice_2", "thinice_3"],
            hp: 3
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //护盾
        id: "C02",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: 0,
        elementHp: 1,
        show: true,
        born: false,
    },
    {
        //石块1
        id: "C031",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["stone_1", "", ""],
            hp: 1,
            eliminate: false,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //石块2
        id: "C032",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["stone_1", "stone_2", ""],
            hp: 2,
            eliminate: false,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //冰块1
        id: "C041",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["ice_1", "", ""],
            hp: 1,
            eliminate: true,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //冰块2
        id: "C042",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["ice_1", "ice_2", ""],
            hp: 2,
            eliminate: true,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //冰块3
        id: "C043",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["ice_1", "ice_2", "ice_3"],
            hp: 3,
            eliminate: true,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //护盾薄冰1
        id: "C051",
        img: "",
        thinIce: {
            img: ["thinice_1", "", ""],
            hp: 1
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: 0,
        elementHp: 1,
        show: true,
        born: false,
    },
    {
        //护盾薄冰2
        id: "C052",
        img: "",
        thinIce: {
            img: ["thinice_1", "thinice_2", ""],
            hp: 2
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: 0,
        elementHp: 1,
        show: true,
        born: false,
    },
    {
        //护盾薄冰3
        id: "C053",
        img: "",
        thinIce: {
            img: ["thinice_1", "thinice_2", "thinice_3"],
            hp: 3
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: 0,
        elementHp: 1,
        show: true,
        born: false,
    },
    {
        //护盾石块1
        id: "C061",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["stone_1", "", ""],
            hp: 1,
            eliminate: false,
        },
        elementID: 0,
        elementHp: 1,
        show: true,
        born: false,
    },
    {
        //护盾石块2
        id: "C062",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["stone_1", "stone_2", ""],
            hp: 2,
            eliminate: false,
        },
        elementID: 0,
        elementHp: 1,
        show: true,
        born: false,
    },
    {
        //巨锚
        id: "C07",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: true,
        },
        elementID: "C07",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //巨锚冰块1
        id: "C131",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["ice_1", "", ""],
            hp: 1,
            eliminate: true,
        },
        elementID: "C07",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //巨锚冰块2
        id: "C132",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["ice_1", "ice_2", ""],
            hp: 2,
            eliminate: true,
        },
        elementID: "C07",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //巨锚冰块3
        id: "C133",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["ice_1", "ice_2", "ice_3"],
            hp: 3,
            eliminate: true,
        },
        elementID: "C07",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //冰块1薄冰1
        id: "C081",
        img: "",
        thinIce: {
            img: ["thinice_1", "", ""],
            hp: 1
        },
        shield: {
            img: ["ice_1", "", ""],
            hp: 1,
            eliminate: true,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //冰块1薄冰2
        id: "C082",
        img: "",
        thinIce: {
            img: ["thinice_1", "thinice_2", ""],
            hp: 2
        },
        shield: {
            img: ["ice_1", "", ""],
            hp: 1,
            eliminate: true,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //冰块1薄冰3
        id: "C083",
        img: "",
        thinIce: {
            img: ["thinice_1", "thinice_2", "thinice_3"],
            hp: 3
        },
        shield: {
            img: ["ice_1", "", ""],
            hp: 1,
            eliminate: true,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //冰块2薄冰1
        id: "C111",
        img: "",
        thinIce: {
            img: ["thinice_1", "", ""],
            hp: 1
        },
        shield: {
            img: ["ice_1", "ice_2", ""],
            hp: 2,
            eliminate: true,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //冰块2薄冰2
        id: "C112",
        img: "",
        thinIce: {
            img: ["thinice_1", "thinice_2", ""],
            hp: 2
        },
        shield: {
            img: ["ice_1", "ice_2", ""],
            hp: 2,
            eliminate: true,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //冰块2薄冰3
        id: "C113",
        img: "",
        thinIce: {
            img: ["thinice_1", "thinice_2", "thinice_3"],
            hp: 3
        },
        shield: {
            img: ["ice_1", "ice_2", ""],
            hp: 2,
            eliminate: true,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //冰块3薄冰1
        id: "C121",
        img: "",
        thinIce: {
            img: ["thinice_1", "", ""],
            hp: 1
        },
        shield: {
            img: ["ice_1", "ice_2", "ice_3"],
            hp: 3,
            eliminate: true,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //冰块3薄冰2
        id: "C122",
        img: "",
        thinIce: {
            img: ["thinice_1", "thinice_2", ""],
            hp: 2
        },
        shield: {
            img: ["ice_1", "ice_2", "ice_3"],
            hp: 3,
            eliminate: true,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //冰块3薄冰3
        id: "C123",
        img: "",
        thinIce: {
            img: ["thinice_1", "thinice_2", "thinice_3"],
            hp: 3
        },
        shield: {
            img: ["ice_1", "ice_2", "ice_3"],
            hp: 3,
            eliminate: true,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //石块1薄冰1
        id: "C091",
        img: "",
        thinIce: {
            img: ["thinice_1", "", ""],
            hp: 1
        },
        shield: {
            img: ["stone_1", "", ""],
            hp: 1,
            eliminate: false,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //石块1薄冰2
        id: "C092",
        img: "",
        thinIce: {
            img: ["thinice_1", "thinice_2", ""],
            hp: 2
        },
        shield: {
            img: ["stone_1", "", ""],
            hp: 1,
            eliminate: false,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //石块1薄冰3
        id: "C093",
        img: "",
        thinIce: {
            img: ["thinice_1", "thinice_1", "thinice_3"],
            hp: 3
        },
        shield: {
            img: ["stone_1", "", ""],
            hp: 1,
            eliminate: false,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //石块2薄冰1
        id: "C101",
        img: "",
        thinIce: {
            img: ["thinice_1", "", ""],
            hp: 1
        },
        shield: {
            img: ["stone_1", "stone_2", ""],
            hp: 2,
            eliminate: false,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //石块2薄冰2
        id: "C102",
        img: "",
        thinIce: {
            img: ["thinice_1", "thinice_2", ""],
            hp: 2
        },
        shield: {
            img: ["stone_1", "stone_2", ""],
            hp: 2,
            eliminate: false,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //石块2薄冰3
        id: "C103",
        img: "",
        thinIce: {
            img: ["thinice_1", "thinice_2", "thinice_3"],
            hp: 3
        },
        shield: {
            img: ["stone_1", "stone_2", ""],
            hp: 2,
            eliminate: false,
        },
        elementID: 0,
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "A01",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "A01",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "A02",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "A02",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "A03",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "A03",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "A04",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "A04",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "A05",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "A05",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "A06",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "A06",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "B011",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "B011",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "B021",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "B021",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "B031",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "B031",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "B04",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "B04",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "B012",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "B012",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "B022",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "B022",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "B031",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "B032",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "B013",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "B013",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "B023",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "B023",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "B033",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "B033",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "B014",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "B014",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "B024",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "B024",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "B034",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "B034",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "B015",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "B015",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "B025",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "B025",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "B035",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "B035",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "B016",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "B016",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "B026",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "B026",
        elementHp: 0,
        show: true,
        born: false,
    },
    {
        //普通格子
        id: "B036",
        img: "",
        thinIce: {
            img: ["", "", ""],
            hp: 0
        },
        shield: {
            img: ["", "", ""],
            hp: 0,
            eliminate: false,
        },
        elementID: "B036",
        elementHp: 0,
        show: true,
        born: false,
    },
];
DataMgr.latticeDtMgr = {
    getDataByID: function (id) {
        for (var i = 0; i < latticeDts.length; i++) {
            if (latticeDts[i].id == id) {
                return latticeDts[i];
            }
        }
        return null;
    },
    getDataLeng: function () {
        return latticeDts.length;
    },
    getDataByInx: function (id) {
        if (id < latticeDts.length) {
            return latticeDts[id];
        }
        return null;
    },
};


//地图配置
var mapDts = [
    {
        id: "4000",     //地图id
        mapX: 8,        //map对应的纵向个数，实际地图会去掉最上面一行出生点
        mapY: 7,        //map对应的横向个数
        map: [
            [0, 0, 3, 3, 3, 0, 0],      //0：透明格子，1：普通格子，2：随机格子，3：出生点
            [0, 3, 1, 1, 1, 3, 0],
            [3, 1, 1, 1, 1, 1, 3],
            [1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1],
            [0, 1, 1, 1, 1, 1, 0],
            [0, 0, 1, 1, 1, 0, 0],
        ]
    },
    {
        id: "4008",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 0, 0, 3, 3, 0, 0, 0],
            [0, 3, 3, 1, 1, 3, 3, 0],
            [0, "C011", "C011", "C011", "C011", "C011", "C011", 0],
            [3, "C011", "C011", "C011", "C011", "C011", "C011", 3],
            [1, "C011", "C011", "C011", "C011", "C011", "C011", 1],
            [1, "C011", "C011", "C011", "C011", "C011", "C011", 1],
            [0, "C011", "C011", "C011", "C011", "C011", "C011", 0],
            [0, "C011", "C011", "C011", "C011", "C011", "C011", 0],
            [0, 0, 0, 1, 1, 0, 0, 0]
        ]
    },
    {
        id: "4010",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 3, 3, 3, 3, 3, 3, 0],
            [0, 1, "C011", 1, 1, "C011", 1, 0],
            [3, 0, "C011", 1, 1, "C011", 0, 3],
            ["C011", "C011", "C011", "C011", "C011", "C011", "C011", "C011"],
            [1, 1, "C011", "C011", "C011", "C011", 1, 1],
            [1, 1, "C011", "C011", "C011", "C011", 1, 1],
            ["C011", "C011", "C011", "C011", "C011", "C011", "C011", "C011"],
            [0, 0, "C011", 1, 1, "C011", 0, 0],
            [0, 1, "C011", 1, 1, "C011", 1, 0]
        ]
    },
    {
        id: "4011",
        mapX: 8,
        mapY: 8,
        map: [
            [0, 0, 3, 3, 3, 3, 0, 0],
            [0, 3, 1, 1, 1, 1, 3, 0],
            [3, 1, "C011", "C011", "C011", "C011", 1, 3],
            [1, 1, "C011", "C081", "C081", "C011", 1, 1],
            [1, 1, "C011", "C081", "C081", "C011", 1, 1],
            [1, 1, "C011", "C081", "C081", "C011", 1, 1],
            [0, 1, "C011", "C081", "C081", "C011", 1, 0],
            [0, 1, "C011", "C011", "C011", "C011", 1, 0],
        ]
    },
    {
        id: "4012",
        mapX: 8,
        mapY: 7,
        map: [
            [3, 3, 3, 3, 3, 3, 3],
            ["C011", "C011", "C011", "C011", "C011", "C011", "C011"],
            ["C011", "C011", "C011", "C011", "C011", "C011", "C011"],
            [0, 1, "C011", 1, "C011", 1, 0],
            [1, "C011", "C011", "C011", "C011", "C011", 1],
            [0, 1, "C011", 1, "C011", 1, 0],
            ["C011", "C011", "C011", "C011", "C011", "C011", "C011"],
            ["C011", "C011", "C011", "C011", "C011", "C011", "C011"],
        ]
    },
    {
        id: "4013",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 0, 3, 3, 3, 3, 0, 0],
            [0, 3, 1, 1, 1, 1, 3, 0],
            [3, 1, 1, 1, 1, 1, 1, 3],
            ["C042", 1, 1, 1, 1, 1, 1, "C042"],
            ["C042", 1, 1, 1, 1, 1, 1, "C042"],
            ["C042", 1, 1, 1, 1, 1, 1, "C042"],
            ["C042", 1, 1, 1, 1, 1, 1, "C042"],
            ["C042", 1, 1, 1, 1, 1, 1, "C042"],
            [0, "C042", "C042", "C042", "C042", "C042", "C042", 0]
        ]
    },
    {
        id: "4014",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 3, 0, 3, 3, 0, 3, 3],
            [1, 1, 3, 1, 1, 3, 1, 1],
            [1, "C011", "C011", "C011", "C011", "C011", "C011", 1],
            [1, "C011", "C011", "C011", "C011", "C011", "C011", 1],
            [0, "C011", "C011", "C011", "C011", "C011", "C011", 0],
            [0, "C011", "C011", "C011", "C011", "C011", "C011", 0],
            [0, "C011", "C011", "C011", "C011", "C011", "C011", 0],
            [1, "C011", "C011", "C011", "C011", "C011", "C011", 1],
            [1, 1, 1, 1, 1, 1, 1, 1]
        ]
    },
    {
        id: "4016",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 0, 3, 3, 3, 3, 0, 0],
            [0, 0, 1, 1, 1, 1, 0, 0],
            [3, 3, 1, "C011", "C011", 1, 3, 3],
            [1, 1, "C011", "C011", "C011", "C011", 1, 1],
            [1, "C011", "C011", "C011", "C011", "C011", "C011", 1],
            [1, "C011", "C011", "C011", "C011", "C011", "C011", 1],
            [1, "C011", "C011", "C011", "C011", "C011", "C011", 1],
            [0, 0, "C011", "C011", "C011", "C011", 0, 0],
            [0, 0, 1, "C011", "C011", 1, 0, 0]
        ]
    },
    {
        id: "4017",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 3, 3, 3, 3, 3, 3, 0],
            [0, 1, 1, "C011", "C011", 1, 1, 0],
            [0, 0, "C011", "C011", "C011", "C011", 0, 0],
            [3, 1, "C012", "C012", "C012", "C012", 1, 3],
            [1, "C011", "C011", "C011", "C011", "C011", "C011", 1],
            [1, "C011", "C011", "C011", "C011", "C011", "C011", 1],
            [0, 1, "C012", "C012", "C012", "C012", 1, 0],
            [0, 0, "C011", "C011", "C011", "C011", 0, 0],
            [0, 1, 1, "C011", "C011", 1, 1, 0]
        ]
    },
    {
        id: "4018",
        mapX: 9,
        mapY: 6,
        map: [
            [3, 3, 3, 3, 3, 3],
            [1, 1, 1, 1, 1, 1],
            [1, 1, "C07", "C07", 1, 1],
            [1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1]
        ]
    },
    {
        id: "4019",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 3, 3, 3, 3, 3, 3, 3],
            [3, "C07", 1, 1, 1, 1, "C07", 1],
            [1, 1, "C011", "C011", "C011", "C011", "C011", 1],
            [1, "C011", "C011", "C011", "C011", "C011", "C011", 1],
            [1, "C011", "C011", 1, 0, "C011", "C011", 1],
            [1, "C011", "C011", 0, 1, "C011", "C011", 1],
            [1, "C011", "C011", "C011", "C011", "C011", "C011", 1],
            [1, "C011", "C011", "C011", "C011", "C011", 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 0]
        ]
    },
    {
        id: "4020",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 3, 3, 0, 0, 3, 3, 0],
            [0, 1, "C07", 3, 3, "C07", 1, 0],
            [0, "C011", "C011", "C011", "C011", "C011", "C011", 0],
            [3, "C011", "C011", "C011", "C011", "C011", "C011", 3],
            [1, "C011", "C011", "C011", "C011", "C011", "C011", 1],
            [1, "C011", "C011", "C011", "C011", "C011", "C011", 1],
            [0, "C011", "C011", "C011", "C011", "C011", "C011", 0],
            [0, "C011", "C011", "C011", "C011", "C011", "C011", 0],
            [0, 1, 1, 1, 1, 1, 1, 0]
        ]
    },
    {
        id: "4021",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 3, 3, 3, 3, 3, 3, 0],
            [3, "C011", "C07", 1, 1, "C07", "C011", 3],
            [1, "C011", 1, 1, 1, 1, "C011", 1],
            [1, "C011", 1, 1, 1, 1, "C011", 1],
            [1, "C011", 1, 1, 1, 1, "C011", 1],
            ["C011", "C011", "C011", 1, 1, "C011", "C011", "C011"],
            ["C011", "C011", "C011", "C011", "C011", "C011", "C011", "C011"],
            ["C011", "C011", "C011", "C011", "C011", "C011", "C011", "C011"],
            [0, "C011", "C011", 1, 1, "C011", "C011", 0]
        ]
    },
    {
        id: "4022",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 3, 0, 3, 3, 0, 3, 3],
            ["C011", "C07", 3, "C07", "C07", 3, "C07", "C011"],
            ["C011", "C011", 1, "C011", "C011", 1, "C011", "C011"],
            ["C011", "C011", 0, "C011", "C011", 0, "C011", "C011"],
            ["C011", "C011", 0, "C011", "C011", 0, "C011", "C011"],
            ["C011", "C011", 0, "C011", "C011", 0, "C011", "C011"],
            ["C011", "C011", 1, "C011", "C011", 1, "C011", "C011"],
            ["C011", "C011", 1, "C011", "C011", 1, "C011", "C011"],
            ["C011", "C011", 1, "C011", "C011", 1, "C011", "C011"]
        ]
    },
    {
        id: "4023",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 0, 3, 3, 3, 3, 0, 0],
            [0, 3, 1, 1, 1, 1, 3, 0],
            [0, 1, 1, "C02", "C02", 1, 1, 0],
            [3, "C02", "C02", 1, 1, "C02", "C02", 3],
            [1, "C02", 1, 1, 1, 1, "C02", 1],
            [1, "C02", "C02", 1, 1, "C02", "C02", 1],
            [0, 1, 1, 1, 1, 1, 1, 0],
            [0, 1, 1, 1, 1, 1, 1, 0],
            [0, 0, 1, 1, 1, 1, 0, 0]
        ]
    },
    {
        id: "4024",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 3, 3, 3, 3, 3, 3, 0],
            [3, 1, 1, 1, 1, 1, 1, 3],
            [1, "C051", "C02", 0, 0, "C02", "C051", 1],
            [1, "C02", "C012", "C012", "C012", "C012", "C02", 1],
            [1, 0, "C012", "C012", "C012", "C012", 0, 1],
            [1, 0, "C012", "C012", "C012", "C012", 0, 1],
            [1, "C02", "C012", "C012", "C012", "C012", "C02", 1],
            [1, "C051", "C02", 0, 0, "C02", "C051", 1],
            [0, 1, 1, 1, 1, 1, 1, 0]
        ]
    },
    {
        id: "4025",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 0, 3, 0, 0, 3, 0, 3],
            [1, 3, "C011", 3, 3, "C011", 3, 1],
            ["C011", "C051", "C011", "C051", "C051", "C011", "C051", "C011"],
            ["C011", "C051", "C011", "C02", "C02", "C011", "C051", "C011"],
            ["C012", "C012", "C012", "C052", "C052", "C012", "C012", "C012"],
            ["C012", "C012", "C012", "C052", "C052", "C012", "C012", "C012"],
            ["C011", "C051", "C011", "C02", "C02", "C011", "C051", "C011"],
            ["C011", "C051", "C011", "C051", "C051", "C011", "C051", "C011"],
            ["C011", "C051", "C011", 0, 0, "C011", "C051", "C011"]
        ]
    },
    {
        id: "4026",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 0, 3, 3, 3, 3, 3, 3],
            [0, 3, "C011", "C02", 1, 1, 1, 1],
            [3, "C011", "C011", "C02", "C02", "C02", 1, 1],
            ["C011", "C011", "C011", "C012", "C012", "C02", "C02", "C02"],
            ["C011", "C011", "C012", "C012", "C012", "C012", "C011", "C02"],
            ["C02", "C011", "C012", "C012", "C012", "C012", "C011", "C011"],
            ["C02", "C02", "C02", "C012", "C012", "C011", "C011", "C011"],
            [1, 1, "C02", "C02", "C02", "C011", "C011", 0],
            [1, 1, 1, 1, "C02", "C011", 0, 0]
        ]
    },
    {
        id: "4027",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 3, 3, 3, 3, 3, 3, 3],
            [1, 1, 1, "C011", "C011", 1, 1, 1],
            [0, 1, "C011", "C011", "C011", "C011", 1, 0],
            [1, 1, "C011", "C012", "C012", "C011", 1, 1],
            [1, "C011", "C012", "C012", "C012", "C012", "C011", 1],
            ["C011", "C012", "C012", "C012", "C012", "C012", "C012", "C011"],
            ["C011", "C011", "C011", "C012", "C012", "C011", "C011", "C011"],
            [0, "C011", "C011", "C011", "C011", "C011", "C011", 0],
            ["C011", "C011", "C011", "C011", "C011", "C011", "C011", "C011"]
        ]
    },
    {
        id: "4028",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 3, 3, 3, 3, 3, 3, 3],
            [1, 1, "C011", 1, 1, "C011", 1, 1],
            [1, "C011", "C011", 0, 0, "C011", "C011", 1],
            ["C011", "C011", "C011", "C011", "C011", "C011", "C011", "C011"],
            ["C012", "C012", "C012", "C011", "C011", "C012", "C012", "C012"],
            [1, 0, "C012", "C012", "C012", "C012", 0, 1],
            ["C012", "C012", "C012", "C011", "C011", "C012", "C012", "C012"],
            ["C011", "C011", 1, 0, 0, 1, "C011", "C011"],
            [1, "C011", 1, 1, 1, 1, "C011", 1]
        ]
    },
    {
        id: "4029",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 0, 3, 3, 3, 3, 0, 0],
            [0, 3, 1, 1, 1, 1, 3, 0],
            [3, 1, 1, "C031", "C031", 1, 1, 3],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, "C031", "C031", 1, 1, 1],
            [1, 1, 1, "C031", "C031", 1, 1, 1],
            [1, 1, 1, "C031", "C031", 1, 1, 1],
            [0, 1, 1, 1, 1, 1, 1, 0],
            [0, 0, 1, 1, 1, 1, 0, 0]
        ]
    },
    {
        id: "4030",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 0, 3, 3, 3, 3, 0, 0],
            [0, 3, 1, 1, 1, 1, 3, 0],
            [0, 1, 1, 1, 1, 1, 1, 0],
            [3, 1, 1, 1, 1, 1, 1, 3],
            ["C031", "C031", "C031", "C031", "C031", "C031", "C031", "C031"],
            ["C031", "C031", "C031", "C031", "C031", "C031", "C031", "C031"],
            [0, 1, 1, 1, 1, 1, 1, 0],
            [0, 1, 1, 1, 1, 1, 1, 0],
            [0, 0, 1, 1, 1, 1, 0, 0]
        ]
    },
    {
        id: "4031",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 3, 3, 3, 3, 3, 3, 0],
            [0, 1, 1, "C07", "C07", 1, 1, 0],
            [0, 1, 1, 1, 1, 1, 1, 0],
            [0, 1, 1, "C031", "C031", 1, 1, 0],
            [0, 1, 1, "C031", "C031", 1, 1, 0],
            [0, 1, 1, "C031", "C031", 1, 1, 0],
            [0, 1, 1, "C031", "C031", 1, 1, 0],
            [0, 1, 1, 1, 1, 1, 1, 0],
            [0, 1, 1, 1, 1, 1, 1, 0]
        ]
    },
    {
        id: "4032",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 0, 3, 0, 0, 3, 0, 3],
            [1, 3, 1, 3, 3, 1, 3, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, "C091", "C092", "C092", "C091", 1, 1],
            ["C091", "C091", "C092", "C103", "C103", "C092", "C091", "C091"],
            ["C011", "C011", "C092", "C093", "C093", "C092", "C011", "C011"],
            [1, 1, "C011", "C012", "C012", "C011", 1, 1]
        ]
    },
    {
        id: "4033",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 3, 3, 3, 3, 3, 3, 3],
            ["C07", 1, 1, "C07", "C07", 1, 1, "C07"],
            ["C031", 1, 1, 1, 1, 1, 1, "C031"],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, "C032", "C032", 1, 1, 1],
            ["C032", 1, 1, "C031", "C031", 1, 1, "C032"],
            [1, 1, 1, "C032", "C032", 1, 1, 1],
            ["C031", 1, 1, 1, 1, 1, 1, "C031"],
            [1, 1, 1, 1, 1, 1, 1, 1]
        ]
    },
    {
        id: "4034",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 3, 0, 3, 3, 0, 3, 3],
            [1, 1, 3, "C011", "C011", 3, 1, 1],
            [1, "C011", "C011", "C091", "C091", "C011", "C011", 1],
            [0, "C101", "C012", "C012", "C012", "C012", "C101", 0],
            ["C011", "C091", "C012", "C013", "C013", "C012", "C091", "C011"],
            ["C011", "C091", "C012", "C013", "C013", "C012", "C091", "C011"],
            [0, "C091", "C012", "C012", "C012", "C012", "C091", 0],
            [1, "C011", "C101", "C101", "C101", "C101", "C011", 1],
            [1, 1, 0, "C011", "C011", 0, 1, 1]
        ]
    },
    {
        id: "4035",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 3, 3, 3, 3, 3, 3, 0],
            [0, 1, "C011", 1, 1, "C011", 1, 0],
            [3, 1, "C091", "C011", "C011", "C091", 1, 3],
            ["C011", "C011", "C092", "C011", "C011", "C092", "C011", "C011"],
            [0, "C011", "C091", "C013", "C013", "C091", "C011", 0],
            [0, "C011", "C091", "C013", "C013", "C091", "C011", 0],
            ["C011", "C011", "C092", "C011", "C011", "C092", "C011", "C011"],
            [0, 1, "C091", "C011", "C011", "C091", 1, 0],
            [0, 1, "C011", 1, 1, "C011", 1, 0]
        ]
    },
    {
        id: "4036",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 3, 3, 0, 0, 3, 3, 3],
            ["C011", "C011", "C011", 3, 3, "C011", "C011", "C011"],
            ["C011", 0, "C012", "C011", "C011", "C012", 0, "C011"],
            ["C012", "C012", "C013", 1, 1, "C013", "C012", "C012"],
            [1, "C011", 1, "C013", "C013", 1, "C011", 1],
            [1, "C011", 1, "C013", "C013", 1, "C011", 1],
            ["C012", "C012", "C013", 1, 1, "C013", "C012", "C012"],
            ["C011", 0, "C012", "C011", "C011", "C012", 0, "C011"],
            [1, 1, 1, 0, 0, 1, 1, 1]
        ]
    },
    {
        id: "4037",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 3, 3, 3, 3, 3, 3, 3],
            ["C061", "C061", 1, "C061", "C061", 1, "C061", "C061"],
            ["C061", "C061", 1, "C061", "C061", 1, "C061", "C061"],
            ["C061", "C061", 1, "C061", "C061", 1, "C061", "C061"],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [0, 1, 1, 1, 1, 1, 1, 0],
            ["C011", "C011", "C011", "C011", "C011", "C011", "C011", "C011"],
            ["C011", "C011", "C011", "C011", "C011", "C011", "C011", "C011"]
        ]
    },
    {
        id: "4038",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 3, 3, 3, 3, 3, 3, 0],
            [0, 1, 1, 1, 1, 1, 1, 0],
            [0, 1, 1, 1, 1, 1, 1, 0],
            [0, 1, 1, 1, 1, 1, 1, 0],
            [0, 1, 1, 1, 1, 1, 1, 0],
            [0, 1, 1, 1, 1, 1, 1, 0],
            [0, 1, 1, 1, 1, 1, 1, 0],
            [0, 1, 1, 1, 1, 1, 1, 0],
            [0, 1, 1, 1, 1, 1, 1, 0]
        ]
    },
    {
        id: "4039",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 3, 0, 3, 3, 0, 3, 3],
            [1, 1, 0, 1, 1, 0, 1, 1],
            [1, 1, 0, 1, 1, 0, 1, 1],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [1, 1, 0, 1, 1, 0, 1, 1],
            [1, 1, 3, 1, 1, 3, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            ["C012", "C012", 0, "C012", "C012", 0, "C012", "C012"],
            ["C012", "C012", 0, "C012", "C012", 0, "C012", "C012"]
        ]
    },
    {
        id: "4040",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 3, 3, 3, 3, 3, 3, 3],
            ["C07", "C07", 1, 1, 1, 1, "C07", "C07"],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 0, 1, 1, 1, 1],
            [1, 1, 1, 1, 0, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1]
        ]
    },
    {
        id: "4041",
        mapX: 8,
        mapY: 8,
        map: [
            [3, 3, 3, 3, 3, 3, 3, 3],
            ["C011", "C011", "C012", "C012", "C012", "C012", "C011", "C011"],
            ["C011", 0, 0, "C012", "C012", 0, 0, "C011"],
            ["C012", 0, "C012", "C012", "C012", "C012", 0, "C012"],
            ["C012", "C012", "C012", "C013", "C013", "C012", "C012", "C012"],
            ["C012", 0, "C012", "C012", "C012", "C012", 0, "C012"],
            ["C011", 0, 0, "C012", "C012", 0, 0, "C011"],
            ["C011", "C011", "C012", "C012", "C012", "C012", "C011", "C011"]
        ]
    },
    {
        id: "4042",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 0, 3, 3, 3, 3, 0, 0],
            [0, 3, "C07", "C07", "C07", "C07", 3, 0],
            [3, 1, "C031", "C031", "C031", "C031", 1, 3],
            [1, 1, 1, "C031", "C031", 1, 1, 1],
            [1, 1, 1, "C031", "C031", 1, 1, 1],
            [1, 1, 1, "C031", "C031", 1, 1, 1],
            [1, 1, 1, "C031", "C031", 1, 1, 1],
            [0, 1, 1, 1, 1, 1, 1, 0],
            [0, 0, 1, 1, 1, 1, 0, 0]
        ]
    },
    {
        id: "4043",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 3, 3, 3, 3, 3, 3, 0],
            [3, 1, 1, 1, 1, 1, 1, 3],
            ["C02", 1, 1, "C031", "C031", 1, 1, "C02"],
            [1, "C02", "C031", "C031", "C031", "C031", "C02", 1],
            [1, 1, "C031", "C032", "C032", "C031", 1, 1],
            ["C02", "C02", "C031", "C031", "C031", "C031", "C02", "C02"],
            [0, 1, 1, "C031", "C031", 1, 1, 0],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1]
        ]
    },
    {
        id: "4044",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 0, 3, 3, 3, 3, 0, 0],
            [0, 3, 1, 1, 1, 1, 3, 0],
            [3, 1, 1, 1, 1, 1, 1, 3],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            ["C091", "C051", "C091", "C051", "C091", "C051", "C091", "C051"],
            ["C052", "C092", "C052", "C093", "C052", "C092", "C052", "C092"],
            ["C091", "C051", "C091", "C051", "C091", "C051", "C091", "C051"]
        ]
    },
    {
        id: "4045",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 3, 3, 3, 3, 3, 3, 3],
            ["C011", 1, "C012", 1, 1, "C012", 1, "C011"],
            [1, 0, 1, "C092", "C092", 1, 0, 1],
            ["C012", 1, "C012", "C012", "C012", "C012", 1, "C012"],
            [1, "C092", "C011", 0, 0, "C011", "C092", 1],
            [1, "C092", "C011", 0, 0, "C011", "C092", 1],
            ["C012", 1, "C012", "C011", "C011", "C012", 1, "C012"],
            [1, "C092", 1, "C091", "C091", 1, "C092", 1],
            ["C011", 1, "C012", 1, 1, "C012", 1, "C011"]
        ]
    },
    {
        id: "4046",
        mapX: 9,
        mapY: 7,
        map: [
            [3, 0, 3, 3, 3, 0, 3],
            ["C07", 0, 1, "C07", 1, 0, "C07"],
            [1, 0, 1, 1, 1, 0, 1],
            [0, 0, 0, 0, 0, 0, 0],
            [1, 0, 1, "B015", 1, 0, 1],
            ["B025", 0, 1, "B025", 1, 0, "B025"],
            [0, 0, 0, 0, 0, 0, 0],
            [1, 0, 1, 1, 1, 0, 1],
            [1, 0, 1, 1, 1, 0, 1]
        ]
    },
    {
        id: "4047",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 3, 3, 3, 3, 3, 3, 3],
            ["C02", "C02", "C02", "C02", "C02", "C02", "C02", "C02"],
            ["C07", "C061", "C07", "C062", "C07", "C061", "C07", "C02"],
            ["C02", "C02", "C02", "C02", "C02", "C02", "C02", "C02"],
            [1, 1, "C02", "C02", "C02", "C02", 1, 1],
            [1, 1, 1, "C032", "C032", 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1]
        ]
    },
    {
        id: "4048",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 3, 3, 3, 3, 3, 3, 3],
            ["C011", "C012", "C012", "C012", "C012", "C011", "C111", "C111"],
            ["C012", "C013", "C013", "C012", "C012", "C012", "C011", "C111"],
            ["C012", "C013", "C013", "C013", "C012", "C012", "C012", "C011"],
            ["C012", "C012", "C013", "C121", "C121", "C012", "C012", "C012"],
            ["C012", "C012", "C012", "C121", "C121", "C013", "C012", "C012"],
            ["C011", "C012", "C012", "C012", "C013", "C013", "C013", "C012"],
            ["C111", "C011", "C012", "C012", "C012", "C013", "C013", "C012"],
            ["C111", "C111", "C011", "C012", "C012", "C012", "C012", "C011"]
        ]
    },
    {
        id: "4049",
        mapX: 9,
        mapY: 7,
        map: [
            [3, 3, 3, 3, 3, 3, 3],
            ["A03", "A03", "C031", "A03", "C031", "A03", "A03"],
            ["A03", "A03", "C031", "A03", "C031", "A03", "A03"],
            ["C031", "C031", "C031", "C031", "C031", "C031", "C031"],
            ["A03", 1, "A03", "B04", "A03", 1, "A03"],
            ["A03", 1, "A03", 1, "A03", 1, "A03"],
            ["C031", "C031", "C031", "C031", "C031", "C031", "C031"],
            ["A03", "A03", "C031", "A03", "C031", "A03", "A03"],
            ["A03", "A03", "C031", "A03", "C031", "A03", "A03"]
        ]
    },
    {
        id: "4050",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 0, 3, 3, 3, 3, 0, 0],
            [0, 3, 1, "C02", "C02", 1, 3, 0],
            [3, 1, "C02", 1, 1, "C02", 1, 3],
            [1, "C02", 1, "C02", "C02", 1, "C031", 1],
            ["C02", "B04", "C02", 1, 1, "C02", "B04", "C02"],
            [1, "C02", 1, 1, 1, 1, "C02", 1],
            [1, "C041", "C041", "C042", "C042", "C041", "C041", 1],
            [0, "C02", 1, 1, 1, 1, "C02", 0],
            [0, 1, "C02", "C02", "C02", "C02", 1, 0]
        ]
    },
    {
        id: "4051",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 3, 3, 3, 3, 3, 3, 3],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [0, 1, 1, 1, 1, 1, 1, 0],
            ["C031", 0, 1, 1, 1, 1, 0, "C031"],
        ]
    },
    {
        id: "4052",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 3, 3, 3, 3, 3, 3, 3],
            ["B025", "C011", "C012", "C011", "C011", "C012", "C011", "B015"],
            ["C011", "C013", "C012", "C012", "C011", "C012", "C013", "C011"],
            ["C012", "C012", 0, "C011", "C011", 0, "C012", "C012"],
            ["C011", "C012", "C011", "C013", "C013", "C011", "C012", "C011"],
            ["C011", "C012", "C011", "C013", "C013", "C011", "C012", "C011"],
            ["C012", "C012", 0, "C011", "C011", 0, "C012", "C012"],
            ["C011", "C013", "C012", "C012", "C011", "C012", "C013", "C011"],
            ["B015", "C011", "C012", "C011", "C011", "C012", "C011", "B025"],
        ]
    },
    {
        id: "4053",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 3, 3, 3, 3, 3, 3, 0],
            [3, "C042", "C041", 1, 1, "C041", "C042", 3],
            ["C042", "C041", 1, 1, 1, 1, "C041", "C042"],
            ["C041", 1, 1, 1, 1, 1, 1, "C041"],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            ["C042", 1, 1, 1, 1, 1, 1, "C042"],
            ["C041", "C042", 1, 1, 1, 1, "C042", "C041"],
            [0, "C041", "C042", 1, 1, "C042", "C041", 0],
        ]
    },
    {
        id: "4054",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 3, 3, 3, 3, 3, 3, 3],
            [1, 1, 1, "C133", "C133", 1, 1, 1],
            [1, "C132", 1, 1, 1, 1, "C132", 1],
            [1, 1, 0, 1, 1, 1, 1, 1],
            [1, 1, 1, 0, 0, 1, 1, 1],
            [1, 1, 1, 0, 0, 1, 1, 1],
            [1, 1, 1, "C131", "C131", 0, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
        ]
    },
    {
        id: "4055",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 3, 3, 3, 3, 3, 3, 3],
            [1, "C051", 1, "C031", "C092", 1, "C051", 1],
            [1, 1, "C101", "C092", 0, "C011", 1, 1],
            [1, "C051", 1, 0, "C092", "C032", "C051", 1],
            [1, 1, "C101", "C092", 0, "C051", 1, 1],
            [1, "C051", 1, 0, "C092", "C032", "C051", 1],
            [1, 1, "C101", "C092", 0, "C012", 1, 1],
            [1, "C011", 1, 0, "C092", "C032", "C011", 1],
            [1, 1, "C011", "C092", "C031", "C011", 1, 1],
        ]
    },
    {
        id: "5001",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 3, 3, 3, 3, 3, 3, 3],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2]
        ]
    },
    {
        id: "5002",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 3, 3, 3, 3, 3, 3, 3],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 0, 2, 2, 2, 2, 2, 2],
            [2, 2, 0, 2, 2, 2, 2, 2],
            [2, 2, 2, 0, 2, 2, 2, 2],
            [2, 2, 2, 2, 0, 2, 2, 2],
            [2, 2, 2, 2, 2, 0, 2, 2],
            [2, 2, 2, 2, 2, 2, 0, 2],
            [2, 2, 2, 2, 2, 2, 2, 2]
        ]
    },
    {
        id: "5003",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 3, 0, 3, 3, 0, 3, 3],
            [2, 2, 3, 2, 2, 3, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [0, 2, 2, 2, 2, 2, 2, 0],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [0, 2, 2, 2, 2, 2, 2, 0],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 0, 2, 2, 0, 2, 2]
        ]
    },
    {
        id: "5004",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 3, 3, 3, 3, 3, 3, 3],
            [3, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 0, 2, 2, 2],
            [2, 2, 2, 0, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 0]
        ]
    },
    {
        id: "5005",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 0, 3, 3, 3, 3, 0, 0],
            [0, 3, 2, 2, 2, 2, 3, 0],
            [3, 2, 2, 2, 2, 2, 2, 3],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [0, 2, 2, 2, 2, 2, 2, 0],
            [0, 0, 2, 2, 2, 2, 0, 0]
        ]
    },
    {
        id: "5006",
        mapX: 9,
        mapY: 8,
        map: [
            [3, 3, 3, 3, 3, 3, 3, 3],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [0, 2, 2, 2, 2, 2, 2, 0],
            [2, 0, 2, 2, 2, 2, 0, 2]
        ]
    },
    {
        id: "5007",
        mapX: 9,
        mapY: 8,
        map: [
            [0, 0, 0, 3, 3, 0, 0, 0],
            [3, 3, 0, 2, 2, 0, 3, 3],
            [2, 2, 3, 2, 2, 3, 2, 2],
            [0, 2, 2, 2, 2, 2, 2, 0],
            [0, 2, 2, 2, 2, 2, 2, 0],
            [0, 2, 2, 2, 2, 2, 2, 0],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [2, 0, 0, 2, 2, 0, 0, 2]
        ]
    },
]
DataMgr.mapDtMgr = {
    getDataByID: function (id) {
        for (var i = 0; i < mapDts.length; i++) {
            if (mapDts[i].id == id) {
                return mapDts[i];
            }
        }
        return null;
    },
};

//元素配置
var elementDts = [
    {

        id: "A01",
        name: "A01",
        img: "1",
        state: "noen",
    },
    {
        id: "A02",
        name: "A02",
        img: "2",
        state: "noen",
    },
    {
        id: "A03",
        name: "A03",
        img: "3",
        state: "noen",
    },
    {
        id: "A04",
        name: "A04",
        img: "4",
        state: "noen",
    },
    {
        id: "A05",
        name: "A05",
        img: "5",
        state: "noen",
    },
    {
        id: "A06",
        name: "A06",
        img: "6",
        state: "noen",
    },
    {
        id: "B011",
        name: "A01",
        img: "1",
        state: "line",
    },
    {
        id: "B021",
        name: "A01",
        img: "1",
        state: "column",
    },
    {
        id: "B031",
        name: "A01",
        img: "1",
        state: "cross",
    },
    {
        id: "B04",
        name: "similar",
        img: "similar",
        state: "similar",
    },
    {
        id: "B012",
        name: "A02",
        img: "2",
        state: "line",
    },
    {
        id: "B022",
        name: "A02",
        img: "2",
        state: "column",
    },
    {
        id: "B032",
        name: "A02",
        img: "2",
        state: "cross",
    },
    {
        id: "B013",
        name: "A03",
        img: "3",
        state: "line",
    },
    {
        id: "B023",
        name: "A03",
        img: "3",
        state: "column",
    },
    {
        id: "B033",
        name: "A03",
        img: "3",
        state: "cross",
    },
    {
        id: "B014",
        name: "A04",
        img: "4",
        state: "line",
    },
    {
        id: "B024",
        name: "A04",
        img: "4",
        state: "column",
    },
    {
        id: "B034",
        name: "A04",
        img: "4",
        state: "cross",
    },
    {
        id: "B015",
        name: "A05",
        img: "5",
        state: "line",
    },
    {
        id: "B025",
        name: "A05",
        img: "5",
        state: "column",
    },
    {
        id: "B035",
        name: "A05",
        img: "5",
        state: "cross",
    },
    {
        id: "B016",
        name: "A06",
        img: "6",
        state: "line",
    },
    {
        id: "B026",
        name: "A06",
        img: "6",
        state: "column",
    },
    {
        id: "B036",
        name: "A06",
        img: "6",
        state: "cross",
    },
    {
        id: "C07",
        name: "anchor",
        img: "anchor",
        state: "anchor",
    }

];
DataMgr.elementDtMgr = {
    getDataByID: function (id) {
        for (var i = 0; i < elementDts.length; i++) {
            if (elementDts[i].id == id) {
                return elementDts[i];
            }
        }
        return null;
    },
};

//关卡配置
var doorDts = [
    {
        id: "1",//关卡数
        species: 4,//元素种类
        mapID: 4000,//地图ID
        num: 1,//任务数量
        limit: 20,//步数
        reward: 10,//奖励
        shieldChance: 0,//护盾几率
        child: [        //需要消除的种类
            {
                id: 1,
                type: "A01",    
                img: "1",
                count: 30, //需要消除的该种元素个数

            }
        ],
    },
    {
        id: "2",//关卡数
        species: 4,//元素种类
        mapID: 4000,//地图ID
        num: 2,//任务数量
        limit: 20,//步数
        reward: 10,
        shieldChance: 0,//护盾几率
        child: [
            {
                id: 1,
                type: "A01",
                img: "1",
                count: 50,

            },
            {
                id: 2,
                type: "A03",
                img: "3",
                count: 40,

            }
        ],
    },
    {
        id: "3",//关卡数
        species: 5,//元素种类
        mapID: 4000,//地图ID
        num: 2,//任务数量
        limit: 20,//步数
        reward: 10,
        shieldChance: 0,//护盾几率
        child: [        
            {
                id: 1,
                type: "A02",
                img: "2",
                count: 50,

            },
            {
                id: 2,
                type: "A04",
                img: "4",
                count: 50,

            }
        ],
    },
    {
        id: "4",//关卡数
        species: 5,//元素种类
        mapID: 4000,//地图ID
        num: 2,//任务数量
        limit: 20,//步数
        reward: 10,
        shieldChance: 0,//护盾几率
        child: [
            {
                id: 1,
                type: "A01",
                img: "1",
                count: 60,

            },
            {
                id: 2,
                type: "A02",
                img: "2",
                count: 60,

            },
            {
                id: 3,
                type: "A04",
                img: "4",
                count: 60,

            }
        ],
    },
    {
        id: "5",//关卡数
        species: 6,//元素种类
        mapID: 4000,//地图ID
        num: 3,//任务数量
        limit: 20,//步数
        reward: 10,
        shieldChance: 0,//护盾几率
        child: [
            {
                id: 1,
                type: "A01",
                img: "1",
                count: 70,

            },
            {
                id: 2,
                type: "A02",
                img: "2",
                count: 70,

            },
            {
                id: 3,
                type: "A04",
                img: "3",
                count: 70,
            }
        ],
    },
]
DataMgr.doorDtMgr = {
    getDataByID: function (id) {
        for (var i = 0; i < doorDts.length; i++) {
            if (doorDts[i].id == id) {
                return doorDts[i];
            }
        }
        return null;
    },
    getDataLeng: function () {
        return doorDts.length;
    }
};


module.exports = DataMgr;
// window.DataMgr=DataMgr;