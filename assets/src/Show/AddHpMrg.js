/**
 * 助力弹窗
 */
cc.Class({
    extends: cc.Component,

    properties: {
        scrollView: {
            default: null,
            type: cc.ScrollView
        },
        currHpLab: {
            default: null,
            type: cc.Label
        },
        maxHpLab: {
            default: null,
            type: cc.Label
        },
        item: {
            default: null,
            type: cc.Prefab
        },
        spacing: 0
    },

    start() {
        const self = this;
        cc.vv.netRoot.on("addHp", function () {
            self.addHp();
        }, cc.vv.netRoot);
    },

    onDisable() {
        cc.vv.netRoot.off("addHp");
    },

    init: function (data) {
        this.currHpLab.string = "" + Globals.hp;
        this.maxHpLab.string = "/" + Globals.maxHp;
        
        const content = this.scrollView.content;
        for (let i = 0; i < data.total; i++) { 
            content.children[i].getComponent("Head").init(data.list[i]);
        }
    },
    addHp: function () {
        this.currHpLab.string = "" + Globals.hp;
    },
    onInvitaionClick: function () {
        cc.vv.MusicComment.playSFX("click");
        if (Globals.hp > Globals.maxHp) {
            cc.vv.UIComment.showPrefab("hint", this.node, "Hint", "体力已达上限");
            return;
        }
        cc.vv.WXComment.wxShare(1);
    },

    onCloseClick: function () {
        cc.vv.MusicComment.playSFX("click");
        this.node.removeFromParent();
    }
});
