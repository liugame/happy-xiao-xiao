/**
 * 单个给我助力人头像
 */
cc.Class({
    extends: cc.Component,

    properties: {
        head: {
            type: cc.Sprite,
            default: null
        },
    },

    start() {
    },

    init: function (data) {
        const spriteCom = this.head.getComponent(cc.Sprite), atlas = 'plist/ui';
        if (!data) {
            cc.vv.UIComment.loadSpriteFrame("headDefault", spriteCom, atlas);
            // } else if (data.type == 1) {        //有人助力，但是未授权
            //     cc.vv.UIComment.loadSpriteFrame("headDefaultNoSign", spriteCom, atlas);
            // } else if (data.type == 2) {        //助力并授权
        } else {
            cc.vv.UIComment.loadOriginImg(data + "?aa=aa.jpg", spriteCom);
        }
    },
});
