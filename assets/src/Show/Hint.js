/**
 * 弹出文字
 */
cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },
    init:function(arge){
        this.node.zIndex=5;
        let lableCom = this.node.getComponent(cc.Label);
        if(arge.label){
            lableCom.string=arge.label;
        }
        else{
            lableCom.string=arge;
        }
        if(arge.position){
            this.node.position=arge.position;
        }
        this.action(arge);
    },
    action:function(arge){
        let action;
        if(arge.time&&arge.len){
            action=cc.moveTo(arge.time,cc.v2(this.node.x,this.node.y+arge.len));
        }
        else{
            action=cc.moveTo(1.2,cc.v2(this.node.x,this.node.x+300));
        }
        let callBack = cc.callFunc(function(){
            this.node.removeFromParent();
        }, this);
        this.node.runAction(cc.sequence(action,callBack));
    }
    // update (dt) {},
});
