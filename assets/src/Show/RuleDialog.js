/**
 * 游戏规则弹窗
 */
cc.Class({
    extends: cc.Component,

    properties: {
        scrollView: {
            default: null,
            type: cc.ScrollView
        },
        labRule: {
            default: null,
            type: cc.Label
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {

    },

    start() {

    },
    
    init: function (data) {
        this.labRule.string = data;
        this.labRule._updateRenderData(true);
        this.scrollView.content.height = this.labRule.node.height;
    },

    //关闭页面
    onCloseBtnClick: function () {
        cc.vv.MusicComment.playSFX("click");
        this.node.removeFromParent();
    },
});
