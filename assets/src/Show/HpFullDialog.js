/**
 * 提示弹窗
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labTip: {
            default: null,
            type: cc.Label
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
    },

    start() {

    },

    init: function (msg) {
        this.labTip.string = msg;
    },

    //关闭页面
    onCloseBtnClick: function () {
        cc.vv.MusicComment.playSFX("click");
        this.node.removeFromParent();
    },
});
