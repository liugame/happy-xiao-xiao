/**
 * 随机礼券掉落弹窗
 */
cc.Class({
    extends: cc.Component,

    properties: {
        rewardImg: {
            default: null,
            type: cc.Node
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
    },

    start() {

    },
    init: function(img) {
        const spriteCom = this.rewardImg.getComponent(cc.Sprite);
        cc.vv.UIComment.loadOriginImg(img, spriteCom);
    },
    back: function () {
        cc.vv.MusicComment.playSFX("click");
        cc.director.loadScene("GameMenu");
    },
    onSureClick: function() {
        cc.vv.MusicComment.playSFX("click");
        this.node.removeFromParent();
    }
    // update (dt) {},
});
