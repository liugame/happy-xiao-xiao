/**
 * 退出游戏弹窗
 */
cc.Class({
    extends: cc.Component,

    properties: {
    },

    start() {

    },

    init: function (data) {
        this.data = data;
    },
    onExitClick: function () {
        cc.vv.MusicComment.playSFX("click");
        if (Globals.model == "infinite") {
            cc.vv.LocalData.saveTzScore();
        }
        cc.director.loadScene('GameMenu');
    },
    onGoOnClick: function () {
        cc.vv.MusicComment.playSFX("click");
        this.node.removeFromParent();
    },
});
