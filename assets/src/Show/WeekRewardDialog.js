/**
 * 上周领取奖励弹窗
 */
cc.Class({
    extends: cc.Component,

    properties: {
        rewardImg: {
            default: null,
            type: cc.Node
        },
        labRank: {
            default: null,
            type: cc.Label
        }
    },

    onLoad() {
        cc.vv.netRoot.on("goToMall", data => {
            this.node.removeFromParent();
        });
    },
    onDisable() {
        cc.vv.netRoot.off("goToMall");
    },

    start() {

    },
    init: function (data) {
        this.data = data;
        const spriteCom = this.rewardImg.getComponent(cc.Sprite);
        cc.vv.UIComment.loadOriginImg(data.gift_img, spriteCom);
        this.labRank.string = `上周排名挑战第${data.index}名`;
    },
    onGoToMallClick: function () {
        cc.vv.MusicComment.playSFX("click");
        cc.vv.WXComment.wxGameGotoMain(this.data.path);
    },

    //关闭页面
    onCloseBtnClick: function () {
        cc.vv.MusicComment.playSFX("click");
        this.node.removeFromParent();
    },
    // update (dt) {},
});
