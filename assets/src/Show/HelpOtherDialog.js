/**
 * 给别人助力弹窗
 */
cc.Class({
    extends: cc.Component,

    properties: {
    },

    start() {

    },

    init: function (canHelpType) {
        this.canHelpType = canHelpType;
    },
    onHelpClick: function () {
        cc.vv.MusicComment.playSFX("click");
        //type=>1.助力+体力，2.助力+锤子，3.助力+炸弹，4.邀请新好友+体力
        const type = Globals.shareQuery.type, beHelpedId = Globals.shareQuery.beHelpedId;
        if (this.canHelpType == 1) {
            Globals.helpedData[type] = [beHelpedId];
        } else if (this.canHelpType == 2) {
            Globals.helpedData[type].push(beHelpedId);
        }
        console.log("点击助力", Globals.shareQuery);
        if (type == 1) {
            cc.vv.JJSdk.getPower(beHelpedId, res => { })
        } else if (type == 2 || type == 3) {
            cc.vv.JJSdk.getProp(beHelpedId, type - 1, res => { })
        } else {
            cc.vv.JJSdk.getPower(beHelpedId, res => { })
        }
        this.node.removeFromParent();
    },
});
