/**
 * 体力不足弹窗
 */
cc.Class({
    extends: cc.Component,

    properties: {
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {

    },

    start() {

    },

    init: function (data) {
        
    },

    //关闭页面
    onCloseBtnClick: function () {
        cc.vv.MusicComment.playSFX("click");
        cc.director.loadScene('GameMenu');
    },

    onShareClick: function() {
        cc.vv.MusicComment.playSFX("click");
        cc.vv.WXComment.wxShare(1);
    }
});
