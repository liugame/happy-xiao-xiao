/**
 * 背景图片加载类
 */
cc.Class({
    extends: cc.Component,

    properties: {
        
    },
    onLoad () {
        this.changeBg();
    },

    start () {
        var self=this;
        cc.vv.netRoot.on("Bg", function(data){    
           self.changeBg(data);
        },cc.vv.netRoot);
    },
    onDestroy(){
        cc.vv.netRoot.off("Bg");
    },
    changeBg:function(data){
        let name;
        if(data){
            if(data.path){
                name=data.path;
                cc.vv.LocalData.setData("bgPath",name);
            }
            else{
                name=data;
            }
        }
        else{
            name=cc.vv.LocalData.getData("bgPath");
        }
        let spriteCom = this.node.getComponent(cc.Sprite);
        cc.vv.UIComment.loadSpriteFrame(name,spriteCom);
    },
    // update (dt) {},
});
