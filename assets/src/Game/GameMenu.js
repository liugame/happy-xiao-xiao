const { type } = require("os");

cc.Class({
    extends: cc.Component,

    properties: {
        musicN: {
            type: cc.Node,
            default: null
        },
        musicImg: {
            type: cc.SpriteFrame,
            default: []
        },
    },

    // onLoad () {},

    start() {
        this.init();
        var self = this;
        cc.vv.netRoot.on("videoWin_activity", function () {
            self.callBack();
        }, cc.vv.netRoot);
        cc.vv.netRoot.on("shareWin_activity", function (arge) {
            self.shareWin(arge);
        }, cc.vv.netRoot);
        cc.vv.netRoot.on("wxPlatformOff", function () {
            cc.vv.WXComment.wxPlatformOff(self.node);
        });

        cc.vv.netRoot.on("netError", function (data) {
            self.netError(data);
        }, cc.vv.netRoot);

        cc.vv.netRoot.on("helpOther", function (canHelpType) {
            self.helpOther(canHelpType);
        }, cc.vv.netRoot);

        cc.vv.netRoot.on("weekRankReward", (res) => {
            cc.vv.UIComment.showPrefab("WeekRewardDialog", this.node, "WeekRewardDialog", res.data);
        }, cc.vv.netRoot);

        cc.vv.netRootjs.helpDialog();
    },
    onDisable() {
        cc.vv.netRoot.off("videoWin_activity");
        cc.vv.netRoot.off("shareWin_activity");
        cc.vv.netRoot.off("wxPlatformOff");
        cc.vv.netRoot.off("netError");
        cc.vv.netRoot.off("helpOther");
        cc.vv.netRoot.off("weekRankReward");
        cc.vv.WXComment.wxPlatformOff(this.node);
    },
    init: function () {
        Globals.shareCount = 0;
        this.changeMusic();

    },
    //-----------音乐--------------------------------
    music: function () {
        let move = cc.vv.LocalData.getData("music");
        if (move == 0) {
            cc.vv.MusicComment.playSFX("click");
            cc.vv.LocalData.setData("music", 1);
            cc.vv.MusicComment.setBGMVolume(1);
        }
        else {
            cc.vv.LocalData.setData("music", 0);
            cc.vv.MusicComment.setBGMVolume(0);
        }
        this.changeMusic();
    },
    changeMusic: function () {
        let move = cc.vv.LocalData.getData("music");
        if (move == 0) {
            this.musicN.getComponent(cc.Sprite).spriteFrame = this.musicImg[0];
        }
        else {
            this.musicN.getComponent(cc.Sprite).spriteFrame = this.musicImg[1];
        }
    },
    netError: function (data) {
        if (typeof data == "object" && data.result == 404) {
            cc.vv.UIComment.showPrefab("HpFullDialog", this.node, "HpFullDialog", data.msg);
        } else {
            cc.vv.UIComment.showPrefab("hint", this.node, "Hint", data);
        }
    },
    helpOther: function (canHelpType) {
        cc.vv.UIComment.showPrefab("HelpOtherDialog", this.node, "HelpOtherDialog", canHelpType);
    },
    //------------------音乐over---------------


    buttonInfinite: function () {
        cc.vv.MusicComment.playSFX("click");
        // let Rondom = Math.floor(Math.random() * Globals.mapInfinite.length);

        // Globals.mapID = Globals.mapInfinite[Rondom];     //只有4000一张图，就不随机地图了

        ///TODO:
        // Globals.door = "infinite";
        // Globals.model = "infinite";
        // Globals.lastSaveScore = Globals.infiniteScore = 0;
        // Globals.infiniteDoor = Math.ceil(Globals.infiniteScore / Enum.infiniteScore);
        // Globals.elementNum = 4;

        // Globals.doorlimit = 0;
        // Globals.shieldChance = 0;
        // cc.director.loadScene("GameInfinite");
        // return;
        //挑战模式不扣除体力
        cc.vv.JJSdk.challengeindex(res => {
            Globals.door = "infinite";
            Globals.model = "infinite";
            Globals.lastSaveScore = Globals.infiniteScore = res.data.score || 0;
            Globals.infiniteDoor = Math.ceil(Globals.infiniteScore / Enum.infiniteScore);
            Globals.elementNum = 4;

            Globals.hp = res.data.power;
            Globals.malletNum = res.data.hammer_num;
            Globals.boomNum = res.data.bomb_num;

            Globals.doorlimit = 0;
            Globals.shieldChance = 0;
            cc.director.loadScene("GameInfinite");
        })
    },
    /**闯关模式 */
    buttonScene: function () {
        cc.vv.MusicComment.playSFX("click");
        cc.vv.JJSdk.getUserInfo(res => {
            Globals.hp = res.data.power;
            Globals.malletNum = res.data.hammer_num;
            Globals.boomNum = res.data.bomb_num;
            cc.vv.JJSdk.cgchoose(res => {
                Globals.maxDoor = res.data.length;
                cc.vv.UIComment.showPrefab("doorMgr", this.node);
            });
        })
    },
    buttonMyRewards: function () {
        cc.vv.MusicComment.playSFX("click");
        cc.vv.JJSdk.mycard((res) => {
            cc.vv.UIComment.showPrefab("RewardMrg", this.node, "RewardMrg", res.data);
        })
    },
    buttonRule: function () {
        cc.vv.MusicComment.playSFX("click");
        cc.vv.JJSdk.rule(res => {
            cc.vv.UIComment.showPrefab("RuleDialog", this.node, "RuleDialog", res.data);
        });
    },
    //每日任务点击
    onTaskClick: function () {
        cc.vv.MusicComment.playSFX("click");
        cc.vv.JJSdk.everydayTask(res => {
            cc.vv.UIComment.showPrefab("DayTaskDialog", this.node, "DayTaskDialog", res.data);
        })
    },
    // update (dt) {},
});
