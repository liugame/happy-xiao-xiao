cc.Class({
    extends: cc.Component,

    properties: {
        propgress: cc.Sprite,
        targeN: cc.Node,
        tipNode: cc.Node,
        bgNode: cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.tipShow = false;
        this.count = 0;     //保证授权完+进度加载完
        this.time = 2;
        this.targe = 0;
        this.maxWidth = this.propgress.node.getParent().width;
    },

    onDisable() {
        cc.vv.netRoot.off("noScopeUserInfo");
    },

    start() {
        cc.vv.netRoot.on("noScopeUserInfo", () => {
            this.tipShow = true;
        }, cc.vv.netRoot);

        let spriteCom = this.bgNode.getComponent(cc.Sprite);
        cc.vv.UIComment.loadSpriteFrame("images/bg_5/bg0", spriteCom);
        
        cc.vv.UIComment.loadSpriteAtlas("plist/oceanPlist");

        if (typeof (wx) == "undefined") {
            Globals.hp = 100;
            Globals.malletNum = 10;
            Globals.boomNum = 10;
            this.count++;
        } else {
            cc.vv.JJSdk.login(() => {
                if (this.tipShow && this.targe >= this.time) {
                    this.tipNode.active = true;

                    let action1 = cc.fadeTo(1, 0); //0.25秒透明度从255降到0
                    let action2 = cc.fadeTo(1, 255); //0.25秒透明度从255降到0
                    let seq = cc.sequence(action1, action2);
                    let req = cc.repeatForever(seq)
                    this.tipNode.runAction(req);
                }
                cc.vv.JJSdk.getUserInfo((res) => {
                    Globals.hp = +res.data.power;
                    Globals.malletNum = +res.data.hammer_num;
                    Globals.boomNum = +res.data.bomb_num;

                    this.count++;
                    this.gotoMenu();

                    /**签到 */
                    cc.vv.JJSdk.signIn(1, res => { });
                });
            });
        }
    },

    gotoMenu: function () {
        if (this.count == 2) {
            cc.director.loadScene('GameMenu');
        }
    },

    update(dt) {
        if (this.targe >= this.time) {
            return;
        }
        this.targe += dt;
        if (this.targe >= this.time) {
            this.propgress.node.width = this.maxWidth;
            this.propgress.node.getParent().active = false;
            if (this.tipShow) {
                this.tipNode.active = true;

                let action1 = cc.fadeTo(1, 0); //0.25秒透明度从255降到0
                let action2 = cc.fadeTo(1, 255); //0.25秒透明度从255降到0
                let seq = cc.sequence(action1, action2);
                let req = cc.repeatForever(seq)
                this.tipNode.runAction(req);
            }
            this.count++;
            this.gotoMenu();
        }
        else {
            const scale = this.targe / this.time;
            this.propgress.node.width = scale * this.maxWidth;
            this.targeN.x = (scale - 0.5) * this.maxWidth;
        }
    },
});
