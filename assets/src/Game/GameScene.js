/**
 * 游戏界面脚本，无尽模式和普通模式公用，用Globals.model判断
 */
cc.Class({
    extends: cc.Component,

    properties: {
        musicN: {
            type: cc.Node,
            default: null
        },
        imgMusic: {
            type: cc.SpriteFrame,
            default: []
        },
        bgShowN: {
            type: cc.Node,
            default: null
        },
        labMalletNum: { //锤子数量
            type: cc.Label,
            default: null
        },
        labBoomNum: {   //炸弹数量
            type: cc.Label,
            default: null
        },
        labLv: {        //第几关
            type: cc.Label,
            default: null
        },
        labHp: {        //体力
            type: cc.Label,
            default: null
        },
        imgReward: {
            type: cc.Node,
            default: null
        },
        labScore: {     //无尽模式分数
            type: cc.Label,
            default: null
        },
        nodeBottom: {
            type: cc.Node,
            default: null
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.bgShowN.active = false;

        this.changeMusic();

        if (cc.vv.JJSdk.isIphoneX()) {
            this.nodeBottom.getComponent(cc.Widget).bottom = 80;
        }
    },

    start() {
        var self = this;
        Globals.boom = Globals.mallet = false;
        if (Globals.model == "infinite") {
        } else if (Globals.model == "scene") {
            self.labLv.string = "第 " + Globals.door + " 关";
            self.labHp.string = Globals.hp + "/" + Globals.maxHp;
            self.imgReward.getParent().active = Globals.maxDoor <= Globals.door;

            const spriteCom = self.imgReward.getComponent(cc.Sprite);
            cc.vv.UIComment.loadOriginImg(Globals.doorRwardUrl, spriteCom);
        }
        Globals.currStep = 0;

        self.labBoomNum.string = Globals.boomNum;
        self.labMalletNum.string = Globals.malletNum;

        //关闭锤子效果
        cc.vv.netRoot.on("shutMallet", function (type) {
            self.shutMallet(type);
            // Globals.mallet=false;
        }, cc.vv.netRoot);
        self.node.on(cc.Node.EventType.TOUCH_START, function (event) {
            // self.doubleN.active=false;
        }, self);
        //修改分数
        cc.vv.netRoot.on("score", function (data) {
            self.setScore(data);
        }, cc.vv.netRoot);
        cc.vv.netRoot.on("addLimitNum", function () {
            self.addLimitNum2();
        }, cc.vv.netRoot);
        //增加体力
        cc.vv.netRoot.on("addHp", function () {
            self.addHp();
        }, cc.vv.netRoot);
        //刷新回调
        // cc.vv.netRoot.on("addRefresh", function () {
        //     self.addRefresh();
        // }, cc.vv.netRoot);
        self.updateScore();
        //增加锤子使用次数回调
        cc.vv.netRoot.on("addMalletNum", function () {
            self.addMalletNum();
        }, cc.vv.netRoot);
        //增加炸弹使用次数回调
        cc.vv.netRoot.on("addBoomNum", function () {
            self.addBoomNum();
        }, cc.vv.netRoot);
        cc.vv.netRoot.on("over", function () {
            self.over();
        }, cc.vv.netRoot);
        cc.vv.netRoot.on("netError", function (data) {
            self.netError(data);
        }, cc.vv.netRoot);
        cc.vv.netRoot.on("GiftFallDialog", data => {
            cc.vv.UIComment.showPrefab("GiftFallDialog", this.node, "GiftFallDialog", data);
        });
        cc.vv.netRoot.on("weekRankReward", (res) => {
            cc.vv.UIComment.showPrefab("WeekRewardDialog", this.node, "WeekRewardDialog", res.data);
        }, cc.vv.netRoot);
    },
    onDestroy() {
        cc.vv.netRoot.off("over");
        cc.vv.netRoot.off("double");
        cc.vv.netRoot.off("shutMallet");
        cc.vv.netRoot.off("score");
        cc.vv.netRoot.off("addLimitNum");
        cc.vv.netRoot.off("addRefresh");
        cc.vv.netRoot.off("addMalletNum");
        cc.vv.netRoot.off("addHp");
        cc.vv.netRoot.off("addBoomNum");
        cc.vv.netRoot.off("netError");
        cc.vv.netRoot.off("GiftFallDialog");
        cc.vv.netRoot.off("weekRankReward");
    },
    updateScore: function () {
        if (Globals.model != "infinite") {
            return;
        }
        this.labScore.string = Globals.infiniteScore + "";
    },
    over: function () {
        cc.vv.UIComment.showPrefab("overWinner", this.node);
    },
    doubleShow: function () {
        return;
    },
    //重新开始
    again: function () {
        cc.vv.MusicComment.playSFX("click");
        if (Globals.model == "infinite") {
            Globals.elementNum = 4;
            Globals.infiniteDoor = 1;
            Globals.doorlimit = 0;
            cc.director.loadScene('GameInfinite');
        }
        else {
            cc.director.loadScene('GameScene');
        }
    },
    //返回菜单
    back: function () {
        cc.vv.MusicComment.playSFX("click");
        cc.director.loadScene('GameMenu');
    },
    //音乐显示
    music: function () {
        let move = cc.vv.LocalData.getData("music");
        if (move == 0) {
            cc.vv.MusicComment.playSFX("click");
            cc.vv.LocalData.setData("music", 1);
            cc.vv.MusicComment.setBGMVolume(1);
        }
        else {
            cc.vv.LocalData.setData("music", 0);
            cc.vv.MusicComment.setBGMVolume(0);
        }
        this.changeMusic();
    },
    //音乐按钮
    changeMusic: function () {
        let move = cc.vv.LocalData.getData("music");
        if (move == 0) {
            this.musicN.getComponent(cc.Sprite).spriteFrame = this.imgMusic[0];
        }
        else {
            this.musicN.getComponent(cc.Sprite).spriteFrame = this.imgMusic[1];
        }
    },
    netError: function (msg) {
        if (typeof msg == "object" && msg.result == 404) {
            cc.vv.UIComment.showPrefab("HpFullDialog", this.node, "HpFullDialog", msg.msg);
        } else {
            cc.vv.UIComment.showPrefab("hint", this.node, "Hint", msg);
        }
    },
    addLimitNum2: function () {
        cc.vv.netRootjs.dispatchEvent("shutMallet");
        cc.vv.netRootjs.dispatchEvent("limitNum", 20);
    },
    addHp: function () {
        this.labHp.string = Globals.hp + "/" + Globals.maxHp;
    },
    //锤子按钮
    mallet: function () {
        cc.vv.MusicComment.playSFX("click");
        cc.vv.netRootjs.dispatchEvent("double");
        if (Globals.boom) {       //如果炸弹效果存在，因为两个道具只能同时使用一个，就不用判断mallet了
            if (Globals.malletNum > 0) {
                Globals.mallet = true;
                Globals.boom = false;
                this.bgShowN.active = true;
            } else {
                cc.vv.UIComment.showPrefab("hint", this.node, "Hint", "道具数量不足");
            }
        } else if (Globals.mallet) {
            this.shutMallet(1);
        } else {
            if (Globals.malletNum > 0) {
                Globals.mallet = true;
                this.bgShowN.active = true;
            } else {
                cc.vv.UIComment.showPrefab("hint", this.node, "Hint", "道具数量不足");
            }
        }
    },
    //炸弹按钮
    boom: function () {
        cc.vv.MusicComment.playSFX("click");
        cc.vv.netRootjs.dispatchEvent("double");
        if (Globals.mallet) {       //如果锤子效果存在，因为两个道具只能同时使用一个，就不用判断boom了
            if (Globals.boomNum > 0) {
                Globals.boom = true;
                Globals.mallet = false;
                this.bgShowN.active = true;
            } else {
                cc.vv.UIComment.showPrefab("hint", this.node, "Hint", "道具数量不足");
            }
        } else if (Globals.boom) {
            this.shutMallet(2);
        } else {
            if (Globals.boomNum > 0) {
                Globals.boom = true;
                this.bgShowN.active = true;
            } else {
                cc.vv.UIComment.showPrefab("hint", this.node, "Hint", "道具数量不足");
            }
        }
    },
    //关闭锤子效果
    shutMallet: function (type) {
        // cc.vv.MusicComment.playSFX("click");
        if (type == 1) {
            if (Globals.mallet == true) {
                Globals.mallet = false;
                Globals.boom = false;
                this.bgShowN.active = false;
            }
        } else {
            if (Globals.boom == true) {
                Globals.boom = false;
                Globals.mallet = false;
                this.bgShowN.active = false;
            }
        }
    },
    setLimit: function () {

    },
    //修改分数
    setScore: function (score) {
        if (score && Globals.model == "infinite") {
            Globals.infiniteScore += score;
            let date = new Date(), day = date.getDay(), hours = date.getHours(), minutes = date.getMinutes(), seconds = date.getSeconds();
            let isNeedSaveScore = !Globals.isSaveLastWeekScore && day == 0 && hours == 23 && minutes == 59 && seconds >= 55 && seconds <= 58;        //周日23点59分55~58秒之间更新一次分数
            if (isNeedSaveScore) {
                Globals.isSaveLastWeekScore = true;
                cc.vv.LocalData.saveTzScore();
            } else if (Globals.infiniteScore - Globals.lastSaveScore >= 200) {
                cc.vv.LocalData.saveTzScore();
            }
            this.updateScore();
        }
    },
    // //无限模式死亡
    // backOver: function () {
    //     Globals.winner = "infinite";
    //     cc.vv.UIComment.showPrefab("overWinner", this.node);
    // },
    //锤子回调
    addMalletNum: function () {
        this.labMalletNum.string = Globals.malletNum;
    },
    addBoomNum: function () {
        this.labBoomNum.string = Globals.boomNum;
    },
    //每日任务点击
    onTaskClick: function () {
        cc.vv.MusicComment.playSFX("click");
        cc.vv.JJSdk.everydayTask(res => {
            cc.vv.UIComment.showPrefab("DayTaskDialog", this.node, "DayTaskDialog", res.data);
        })
    },
    //增加体力点击
    onAddHpClick: function () {
        cc.vv.MusicComment.playSFX("click");
        cc.vv.JJSdk.invatelist(res => {
            cc.vv.UIComment.showPrefab("AddHpMrg", this.node, "AddHpMrg", res.data);
        })
    },
    //回到主页
    onBackClick: function () {
        cc.vv.MusicComment.playSFX("click");
        cc.vv.UIComment.showPrefab("ExitDialog", this.node);
    },
    //规则按钮
    onRuleClick: function () {
        cc.vv.MusicComment.playSFX("click");
        cc.vv.JJSdk.rule(res => {
            cc.vv.UIComment.showPrefab("RuleDialog", this.node, "RuleDialog", res.data);
        })
    },
    //我的奖励
    onMyRewardsClick: function () {
        cc.vv.MusicComment.playSFX("click");
        cc.vv.JJSdk.mycard((res) => {
            cc.vv.UIComment.showPrefab("RewardMrg", this.node, "RewardMrg", res.data);
        })
    },
    onAddMalletClick: function () {
        cc.vv.MusicComment.playSFX("click");
        cc.vv.WXComment.wxShare(2);
    },
    onAddBoomClick: function () {
        cc.vv.MusicComment.playSFX("click");
        cc.vv.WXComment.wxShare(3);
    },
    onRankClick: function () {
        cc.vv.MusicComment.playSFX("click");
        cc.vv.LocalData.saveTzScore(() => {
            cc.vv.UIComment.showPrefab("RankDialog", this.node, "RankDialog", 0);
        });
    }

    // update (dt) {},
});
