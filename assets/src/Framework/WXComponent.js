/**
 * 微信相关api处理
 */
cc.Class({
    extends: cc.Component,

    properties: {

    },

    start() {
        // wx.showShareMenu();//开启右上角转发
        // wx.hideShareMenu();//隐藏右上角转发
    },
    init: function () {
        if (typeof (wx) != "undefined") {
            wx.showShareMenu({
                withShareTicket: true,
                success: function (data) {
                },
                fail: function (data) {
                }
            })
            //右上角
            let title = "离奖励就差一步，快帮一把";
            let imageUrl = "http://anta.cn-bj.ufileos.com/xxl/game001.png";
            var shareTemplate = null;
            // if (cc.vv.JJSdk) {
            //     shareTemplate = cc.vv.JJSdk.getShareTemplates();
            // }
            if (shareTemplate != null && shareTemplate.regular != undefined && shareTemplate.regular.length > 0) {
                let random = Math.floor(Math.random() * shareTemplate.regular.length)
                title = shareTemplate.regular[random].title
                imageUrl = shareTemplate.regular[random].pic
            }
            wx.onShareAppMessage(function () {
                return {
                    title: title,
                    imageUrl: imageUrl,
                }
            })
            wx.onShow(function () {
                if (Globals.shareCount <= 0) {
                    return;
                }
                let shareName = "shareWin"
                if (Globals.shareName) {
                    shareName = "shareWin_" + Globals.shareName;
                }
                let timeStamp = Globals.timeStamp;
                Globals.timeStamp = Date.parse(new Date()) / 1000;
                let time = Globals.timeStamp - timeStamp;
                if (time > 3 && Globals.shareCount > 0) {
                    Globals.shareCount = 0;
                    cc.vv.netRootjs.dispatchEvent(shareName, "win");
                    Globals.shareName = null;
                }
                else {
                    Globals.shareCount = 0;
                    cc.vv.netRootjs.dispatchEvent(shareName, "lose");
                    Globals.shareName = null;
                }
            });

            const info = wx.getSystemInfoSync();
            Globals.safeArea = info.safeArea;
        }
    },
    //微信数据
    wxMessage: function (type, data) {
        if (typeof (wx) != "undefined") {
            var agrs;
            switch (type) {
                case "friends":
                    agrs = { type: "friends_all" };//打开朋友排行榜
                    cc.vv.UIComment.showPrefab("rank", data, "Rank", agrs);
                    break;
                case "group":
                    agrs = { type: "friends_all" };//打开群排行榜
                    cc.vv.UIComment.showPrefab("rank", data, "Rank", agrs);
                    break;
                case "getData":
                    //agrs = { type: "getData",keys ："score"};//获得数据
                    // if (typeof (wx) != "undefined") {
                    //     wx.postMessage(agrs);
                    // }
                    break;
                case "setData":
                    agrs = { type: "saveData", score: data };//存数据
                    if (typeof (wx) != "undefined") {
                        wx.postMessage(agrs);
                    }
                    break;
                default:

                    break;
            }
        }
    },
    //跳转游戏
    wxGameGotoMain: function (path) {
        if (typeof (wx) != "undefined") {
            wx.navigateToMiniProgram({
                appId: "wx25b44eddc868c0f6",
                path: path || "pages/index/index",
                extraData: "",
                envVersion: "release",
                success: function () {
                    cc.vv.netRootjs.dispatchEvent("goToMall");
                    console.log("跳转成功");
                },
                fail: function (err) {
                    console.log("跳转失败", err);
                },
                complete: function () { }

            })
        }
    },
    /**
     * 微信分享
     * @param {*} arge 1.助力+体力，2.助力+锤子，3.助力+炸弹，4.邀请新好友+体力
     */
    wxShare: function (arge) {
        if (arge) {
            Globals.shareName = arge;
        }
        else {
            Globals.shareName = null;
        }
        if (typeof (wx) != "undefined") {
            Globals.shareCount += 1;
            Globals.timeStamp = Date.parse(new Date()) / 1000;
            var shareTemplate = null;
            var title;
            var imageUrl;
            var query = `type=${arge}&beHelpedId=${cc.vv.JJSdk.JJ_MINIGAME_SDK.gameUserInfo.md5_id}`;
            const shareData = [
                {
                    title: "离奖励就差一步，快帮一把",
                    imageUrl: "http://anta.cn-bj.ufileos.com/xxl/game001.png",
                },
                {
                    title: "离奖励就差一步，快帮一把",
                    imageUrl: "http://anta.cn-bj.ufileos.com/xxl/game001.png",
                },
                {
                    title: "离奖励就差一步，快帮一把",
                    imageUrl: "http://anta.cn-bj.ufileos.com/xxl/game001.png",
                },
                {
                    title: "离奖励就差一步，快帮一把",
                    imageUrl: "http://anta.cn-bj.ufileos.com/xxl/game001.png",
                },
            ];
            title = shareData[arge - 1].title;
            imageUrl = shareData[arge - 1].imageUrl;
            wx.shareAppMessage({
                title: title,
                imageUrl: imageUrl,
                query: query,
                success: function () {
                    console.log("分享成功");
                },
                fail: function () {
                    console.log("分享失败");
                }
            })
        }
    },

    //微信震动
    wxVibrate: function () {
        if (typeof (wx) != "undefined") {
            wx.vibrateShort(
                function complete() { },
                function success() { },
                function fail() { }
            );
        }
    },
    //关闭 平台广告
    wxPlatformOff: function (arge) {
        if (typeof (wx) == "undefined" || !Globals.isOpenAd) {
            return;
        }
        cc.vv.JJSdk.hideDrawerAd(arge);
        cc.vv.JJSdk.hideRightFloatAd(arge);
        cc.vv.JJSdk.hideLeftFloatAd(arge);
        cc.vv.JJSdk.hideBannerAd(arge);
    },
    // update (dt) {},
});
