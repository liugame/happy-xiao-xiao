/**
 * UI弹窗管理类
 */
cc.Class({
    extends: cc.Component,

    properties: {
        _atlas: {
            type: cc.SpriteAtlas,
            default: []
        }
    },

    //加载图集资源
    loadSpriteAtlas: function (atlasPath) { //参数说明（图集路径）
        if (atlasPath) {
            if (this._atlas[atlasPath] == undefined) {
                cc.loader.loadRes(atlasPath, cc.SpriteAtlas, function (err, atlas) {
                    this._atlas[atlasPath] = atlas;
                }.bind(this));
            }
        }
    },

    //加载图片资源
    loadSpriteFrame: function (path, nodeSprite, atlasPath) { //参数说明（图片路径，节点的sprite组件,图集路径）
        if (atlasPath) {
            if (this._atlas[atlasPath] == undefined) {
                cc.loader.loadRes(atlasPath, cc.SpriteAtlas, function (err, atlas) {
                    this._atlas[atlasPath] = atlas;
                    let frame = atlas.getSpriteFrame(path);;
                    nodeSprite.spriteFrame = frame;
                }.bind(this));
            }
            else {
                let frame = this._atlas[atlasPath].getSpriteFrame(path);
                nodeSprite.spriteFrame = frame;
            }
        }
        else {
            cc.loader.loadRes(path, cc.SpriteFrame, function (err, spriteFrame) {
                nodeSprite.spriteFrame = spriteFrame;
            })
        }
    },

    //动态加载预制体
    showPrefab: function (layerName, parent, scriptName, agrs) { //参数说明(预制名,父节点,脚本名,参数)
        parent || (parent = cc.vv.netRoot.getParent());
        if (!parent)
            return;
        cc.loader.loadRes("prefab/" + layerName, function (err, prefab) {
            if (layerName != "hint") {
                let layer = parent.getChildByName(layerName);
                if (layer) {
                    if (agrs && layer.getComponent(scriptName)) {
                        let script = layer.getComponent(scriptName);
                        script.init(agrs)
                    }
                    return;
                }
            }

            let layer2 = cc.instantiate(prefab);
            layer2.name = layerName;
            parent.addChild(layer2);
            if (agrs != undefined && layer2.getComponent(scriptName)) {
                let script = layer2.getComponent(scriptName);
                script.init(agrs)
            }
        });
    },

    loadOriginImg: function (url, nodeSprite) {
        cc.loader.load(url, (err, texture) => {
            let sp = new cc.SpriteFrame(texture);
            nodeSprite.spriteFrame = sp;
            sp = null;
        });
    }

});
