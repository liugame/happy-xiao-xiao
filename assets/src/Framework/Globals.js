/**
 * 全局变量
 */
window.Globals = {
    canPlayGame: true,      //是否可玩游戏
    isSaveLastWeekScore: false,     //是否已经保存上周分数了
    shareQuery: {},         //分享的参数
    helpedData: {   //本次进来是否已经给别人助过力了,{ type: （助力类型），[]: 被帮助人md5_id集合 }
        1: [],
        2: [],
        3: [],
        4: []
    },
    safeArea: 0,    //刘海高度
    hp: 10,  //体力
    maxHp: 10,  //体力最大值
    addLimitNeedCostHp: 1,  //增加20步需要消耗体力数
    cost: 1,    //参加一场游戏需要消耗多少体力  ///TODO:这两个建议服务端存储，方便后续改动
    score: 0,//分数
    infiniteScore: 0,   //无尽模式分数
    infiniteDoor: 1,    //无限关卡
    lastSaveScore: 0,   //无尽模式上次传给服务端的值
    mapID: 4000,//地图ID
    door: 1,//关卡
    maxDoor: 0,    //已经闯过的最大关卡 
    doorRwardUrl: "",  //关卡对应的奖励图路径
    elementNum: 3,//元素种类
    maxElementNum: 6,   //元素种类最多几种
    winner: "",//是否通关,winner:胜利,lose:失败 
    doorShow: 1,
    doorLive: 0,//关卡生命
    doorlimit: 0,//步数 
    mallet: false,   //是否选中小木槌道具
    malletNum: 5,    //拥有小木槌数量
    boom: false,     //是否选中炸弹道具
    boomNum: 5,      //拥有炸弹数量
    model: "",//模式选择，取值 infinite | scene
    shieldChance: 30,    //护盾几率（当前没用到）
    currStep: 0,        //当前走过多少步

    eliminateCount: 0,//消除连击
}

