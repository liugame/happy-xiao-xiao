/**
 * 接口上层调用
 */
cc.Class({
    extends: cc.Component,
    properties: {
        drawerAdStatus: "off",
        adList: [],
        leftNode: null,
        rightNode: null,
        drawerNode: null,
        JJ_MINIGAME_SDK: null,
        gameId: 0,
        unionChannelId: 2,
        isinit: false,
        template: ""
    },
    init: function (data, callback) {
        var self = this;
        if (data == undefined) {
            data = {};
        }
        if (data.gameId == undefined) {
            data.gameId = 0;
        }
        if (data.version == undefined) {
            data.version = "1.0.0";
        }
        if (data.channelId == undefined) {
            data.channelId = 0;
        }
        if (self.isinit == true) {

        } else {
            self.isinit = true;
            self.JJ_MINIGAME_SDK = require("sdk");
            callback();
        }
    },
    showModal: function (msg, title, showCancel, confirmText) {
        this.JJ_MINIGAME_SDK.showModal(msg, title, showCancel, confirmText);
    },
    isOpenShare: function () {
        var self = this;
        return self.JJ_MINIGAME_SDK.isOpenShare();
    },
    isOpenBox: function () {
        var self = this;
        return self.JJ_MINIGAME_SDK.isOpenBox();
    },
    login: function (callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.login(function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },

    onShow: function (callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.onShow(function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    onHide: function (callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.onHide(function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },

    onShareAppMessage: function (data) {
        var self = this;
        self.JJ_MINIGAME_SDK.onShareAppMessage(data);
    },
    shareAppMessage: function (data) {
        var self = this;
        self.JJ_MINIGAME_SDK.shareAppMessage(data);
    },

    isIphoneX: function () {
        var size = cc.view.getFrameSize();
        var isIphoneX = false;
        if (size.height / size.width >= 1.9) {
            isIphoneX = true;
        }
        return isIphoneX;
    },

    /**game api start */
    getUserInfo(callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.getUserInfo(function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    weekrank(callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.weekrank(function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    lastweekrank(callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.lastweekrank(function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    getWeekRankCard(callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.getWeekRankCard(function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    challengeindex(callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.challengeindex(function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    /**闯关选择关卡接口 */
    cgchoose(callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.cgchoose(function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    /**闯关模式首页 */
    cgindex(game_id, callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.cgindex(game_id, function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    /**我的卡券 */
    mycard(callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.mycard(function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    /**玩游戏扣减体力 */
    deductpower(callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.deductpower(function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    /**助力好友 获得【体力值】 */
    getPower(share_uid, callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.getPower(share_uid, function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    /**助力好友 获得【小锤，炸弹】 */
    getProp(share_uid, type, callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.getProp(share_uid, type, function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    /**每日任务 */
    everydayTask(callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.everydayTask(function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    /**领取每日任务奖励 */
    getTaskGift(data, callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.getTaskGift(data, function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    /**保存闯关记录数据 */
    saveCgHistory(task_id, callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.saveCgHistory(task_id, function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    /**保存挑战模式分值 */
    saveTzHistory(callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.saveTzHistory(function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    /**随机掉落奖券 */
    rankprize(callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.rankprize(function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    /**我的礼品列表页领取卡券接口 */
    getGiftCard(user_card_id, callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.getGiftCard(user_card_id, function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    /**每日任务签到 */
    signIn(task_id, callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.signIn(task_id, function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    /**游戏界面补充体力 */
    invatelist(callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.invatelist(function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    deductProp(type, callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.deductProp(type, function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    /**规则*/
    rule(callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.rule(function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    /**获取上周有没有排行榜奖励 */
    indexAlert(callback) {
        var self = this;
        self.JJ_MINIGAME_SDK.indexAlert(function (res) {
            if (callback != undefined) {
                callback(res);
            }
        });
    },
    /**game api end */
});