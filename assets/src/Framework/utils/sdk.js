'use strict';

/**
 * 网络请求底层封装
 */
const { callbackify } = require('util');

var JJ_MINIGAME_SDK = {
    ENV: "prod",
    SDKVERSION: "1.0.4",
    JUMP_MINI_PROGRAM_ENV: "release",
    shareMessageFlag: false,
    HOST: "https://anta.yokeneng.com/kt/xxl",
    MD5_KEY: "dE2abfkqe0YTX6p4inhBym18HcUZ9jNG",
    isNet: true,       //是否联网调试
    gameId: 4,
    unionChannelId: 0,
    template: "",
    _caId: 0,
    gameInfo: {
        openShare: "yes",
        openBox: "yes",
        env: "",
        offer_id: 0
    },
    wxLoginCode: "",        //微信login获取的code
    token: "",
    wxUserInfo: {
        userInfo: {},
        rawData: "",
        signature: "",
        encryptedData: "",
        iv: ""
    },
    gameUserInfo: {
        avatarUrl: "",
        bomb_num: 1,
        hammer_num: 1,
        id: 2,
        is_get: 1,
        md5_id: "",
        nickName: "",
        power: "0",
        unionid: "",
        is_get: 0,
        cg: { step_min: 0, step_max: 0 },       //闯关模式掉落奖券的步数范围
        tz: { step_min: 0, step_max: 0 }        //无尽模式掉落奖券的步数范围
    },
    sign: function (data) {
        !data && (data = {});
        let arr = [];
        for (var key in data) {
            arr.push(key);
        }
        arr.sort();
        let str = '';
        for (var i in arr) {
            str += arr[i] + "" + data[arr[i]];
        }
        const md5 = require('md5');
        str = this.MD5_KEY + str + this.MD5_KEY;
        return md5.hex_md5(str);
    },
    showModal: function (msg, title, showCancel, confirmText) {
        wx.showModal({
            title: title || "提示",
            content: msg || "error",
            showCancel: showCancel || false,
            confirmText: confirmText || "我知道了"
        })
    },
    /**
     * 
     * @param {*} options {url}
     * @param {*} auth 
     */
    baseRequest(options, auth = true) {
        if (!this.isNet) {
            options.success && options.success({});
            return;
        }
        const self = this, url = options.url;
        let header = {};
        if (auth) {
            header.Authorization = "Bearer " + self.token;
        }
        cc.vv.netRootjs.showMask();
        wx.request({
            method: "POST",
            data: options.data,
            header,
            url: self.HOST + url,
            success: function (res) {
                console.log("请求成功", url, res);
                const isHelp = url == "/api/getPower" || url == "/api/getProp";
                const isShowErrMsg = ["/api/rankprize", "/api/indexAlert"].indexOf(url) < 0;
                if (url == "/api/indexAlert") {
                    console.log("上周奖励:", res);
                }
                if (res.data.result == 1) {      //成功
                    options.success && options.success(res);
                    isHelp && cc.vv.netRootjs.dispatchEvent("netError", "助力成功");
                } else if (res.data.result == 404) {
                    cc.vv.netRootjs.dispatchEvent("netError", res.data);
                } else {
                    isHelp ?
                        cc.vv.netRootjs.dispatchEvent("netError", res.data.msg || "助力失败") :
                        isShowErrMsg ? cc.vv.netRootjs.dispatchEvent("netError", res.data.msg || "数据异常") : null;
                }
                cc.vv.netRootjs.hideMask();
            },
            fail: function (res) {
                console.log("请求失败", url, res);
                options.fail && options.fail(res);
                cc.vv.netRootjs.hideMask();
            },
            complete: function (res) {
                console.log("请求", url, res);
                options.complete && options.complete(res);
                cc.vv.netRootjs.hideMask();
            }
        })
    },
    getSetting: function (callback) {
        const self = this;
        cc.vv.netRootjs.showMask();
        wx.getSetting({
            success: function (res) {
                if (!res.authSetting['scope.userInfo']) {
                    cc.vv.netRootjs.dispatchEvent("noScopeUserInfo");
                    self.createUserInfoButton(callback);
                } else {
                    wx.getUserInfo({
                        success(res) {
                            cc.vv.netRootjs.hideMask();
                            self.wxUserInfo = res;
                            callback();
                        }
                    })
                }
            }
        })
    },
    createUserInfoButton: function (callback) {
        const self = this,
            size = cc.view.getFrameSize(),
            button = wx.createUserInfoButton({
                type: 'text',
                text: '',
                style: {
                    left: 0,
                    top: 0,
                    width: size.width,
                    height: size.height,
                    lineHeight: 0,
                    backgroundColor: '',
                    color: '#ffffff',
                    textAlign: 'center',
                    fontSize: 16,
                    borderRadius: 4
                }
            });

        button.onTap((res) => {
            if (!res.userInfo) {
                return;
            }
            cc.vv.netRootjs.hideMask();
            self.wxUserInfo = res;
            button.destroy();
            callback();
        });
        button.show();
    },
    login: function (callback) {
        if (!this.isNet) {
            callback({});
            return;
        }
        var self = this;
        cc.vv.netRootjs.active = true;
        var query = Globals.shareQuery;
        if (!query.type) {
            var launchOptions = wx.getLaunchOptionsSync();
            Globals.shareQuery = query = launchOptions && launchOptions.query;
        }
        console.log("login ", Globals.shareQuery);
        wx.login({
            success: function (res) {
                if (res.code) {
                    self.wxLoginCode = res.code;
                    self.getSetting(() => {
                        var data = {
                            "code": res.code,
                            "iv": self.wxUserInfo.iv,
                            "cryptData": self.wxUserInfo.encryptedData
                        };

                        if (query.type) {
                            const beHelpedId = query["beHelpedId"];
                            console.log("获取助力", query, beHelpedId);
                            if (beHelpedId && beHelpedId != self.gameUserInfo.md5_id) {
                                data.share_ui = beHelpedId;
                            }
                        }
                        self.baseRequest({
                            url: "/regist",
                            data: JSON.stringify(data),

                            success: function (res) {
                                if (callback != undefined) {
                                    self.token = res.data.data.token;
                                    callback(res.data)
                                }
                            }
                        }, false)
                    })
                } else {
                    console.log('login error' + res.errMsg)
                }
            }
        })
    },
    /**设置剪切板 */
    setClipboardData: function (data) {
        var self = this;
        wx.setClipboardData({
            data: data,
            success(res) {
                cc.vv.UIComment.showPrefab("hint", cc.vv.netRoot.getParent(), "Hint", "成功复制");
            }
        })
    },
    isOpenShare: function () {
        var self = this;
        if (self.gameInfo.openShare == "yes") {
            return true;
        } else {
            return false;
        }
    },
    // isOpenBox: function () {
    //     var self = this;
    //     if (self.gameInfo.openBox == "yes") {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // },
    /**提交分数 */
    // submitScore: function (score, callback) {
    //     var self = this;
    //     self.baseRequest({
    //         url: "/submitScore",
    //         data: {
    //             SDKVERSION: self.SDKVERSION,
    //             openId: self.userInfo.openId,
    //             score: score,
    //             gameId: self.gameId,
    //         },
    //         success: function (res) {
    //             if (callback != undefined) {
    //                 callback(res.data)
    //             }
    //         }
    //     })

    // },
    // getGoldBalance: function (callback) {
    //     var self = this;
    //     self.baseRequest({
    //         url: "/getGoldBalance",
    //         data: {
    //             SDKVERSION: self.SDKVERSION,
    //             openId: self.userInfo.openId,
    //             gameId: self.gameId,
    //         },
    //         success: function (res) {
    //             if (callback != undefined) {
    //                 callback(res.data)
    //             }
    //         }
    //     })

    // },
    // updateGoldBalance: function (data, callback) {
    //     var self = this;
    //     if (data != undefined && data != null) {
    //         data['openId'] = self.userInfo.openId;
    //         data['gameId'] = self.gameId;
    //         data['SDKVERSION'] = self.SDKVERSION;
    //     }
    //     self.baseRequest({
    //         url: "/updateGoldBalance",
    //         data: data,
    //         success: function (res) {
    //             if (callback != undefined) {
    //                 callback(res.data)
    //             }
    //         }
    //     })

    // },
    // getRank: function (num, callback) {
    //     var self = this;
    //     self.baseRequest({
    //         url: "/rank",
    //         data: {
    //             openId: self.userInfo.openId,
    //             gameId: self.gameId,
    //             num: num
    //         },
    //         success: function (res) {
    //             if (callback != undefined) {
    //                 callback(res.data)
    //             }
    //         }
    //     })

    // },

    // getScore: function (callback) {
    //     var self = this;
    //     self.baseRequest({
    //         url: "/getScore",
    //         data: {
    //             openId: self.userInfo.openId,
    //             gameId: self.gameId
    //         },
    //         success: function (res) {
    //             if (callback != undefined) {
    //                 callback(res.data)
    //             }
    //         }
    //     })
    // },

    onShow: function (callback) {
        var self = this;
        wx.onShow(function (res) {
            var showTime = new Date().getTime();
            var times = showTime - self._hideTime;
            if (self.shareMessageFlag == true) {
                self.doShare();
            }
            self.baseRequest({
                url: "/show",
                data: {
                    openId: self.userInfo.openId,
                    gameId: self.gameId,
                    caId: self._caId,
                    times: times
                },
                success: function (res) {
                    self._caId = 0;
                }
            })
            if (callback) {
                callback(res);
            }
        });
    },

    onHide: function (callback) {
        var self = this;
        wx.onHide(function (res) {
            self._hideTime = new Date().getTime();
            self.baseRequest({
                url: "/hide",
                data: {
                    openId: self.userInfo.openId,
                    gameId: self.gameId
                },
                success: function (res) {
                    if (callback != undefined) {
                        callback(res.data)
                    }
                }
            })
            if (callback) {
                callback(res);
            }
        });
    },
    doShare: function () {
        var self = this;
        self.shareMessageFlag = false;
        self._showTime = new Date().getTime();

        if (self._showTime - self._hideTime >= self._shareMinTime2) {
            self._shareTimes = self._shareTimes + 1;
        }
        if (self._shareTimes >= 3) {
            self._callback(false);
        } else {
            if (self._showTime - self._hideTime >= self._shareMinTime) {
                if (self._callback != undefined && self._callback != null) {
                    self._callback(true);
                }
            } else {

                if (self._callback != undefined && self._callback != null) {
                    self._callback(false);
                }
            }
        }
    },
    // getShareTemplates: function () {
    //     var self = this;
    //     return self.shareInfo;
    // },
    // navigateToMoreGame: function (callback) {
    //     var self = this;
    //     wx.navigateToMiniProgram({
    //         appId: self.moreGameAppId,
    //         path: self.moreGamePath,
    //         extraData: self.moreGameExtraData,
    //         envVersion: self.JUMP_MINI_PROGRAM_ENV,
    //         success: function () {
    //             self.baseRequest({
    //                 url: "/navigateToMoreGame",
    //                 data: {
    //                     gameId: self.gameId,
    //                     openId: self.userInfo.openId,
    //                 },
    //                 success: function (res) {
    //                     if (callback != undefined) {
    //                         callback(res.data);
    //                     }
    //                 }
    //             })
    //         }
    //     });
    // },
    getWxAdInfo: function () {
        var self = this;
        if (self.adInfo.wxAd == undefined) {
            self.adInfo.wxAd = [];
        }
        return self.adInfo.wxAd;
    },
    // getFirstScreenAd: function () {
    //     var self = this;
    //     if (self.adInfo.platAd.screenAd == undefined) {
    //         self.adInfo.platAd.screenAd = [];
    //     }
    //     return self.adInfo.platAd.screenAd;
    // },
    // getRightFloatAd: function (callback) {
    //     var self = this;
    //     if (self.adInfo.platAd.rightFloatAd == undefined) {
    //         self.adInfo.platAd.rightFloatAd = [];
    //     }
    //     return self.adInfo.platAd.rightFloatAd;
    // },
    // getLeftFloatAd: function (callback) {
    //     var self = this;
    //     if (self.adInfo.platAd.leftFloatAd == undefined) {
    //         self.adInfo.platAd.leftFloatAd = [];
    //     }
    //     return self.adInfo.platAd.leftFloatAd;
    // },
    // getDrawerAd: function (callback) {
    //     var self = this;
    //     if (self.adInfo.platAd.drawerAd == undefined) {
    //         self.adInfo.platAd.drawerAd = [];
    //     }
    //     return self.adInfo.platAd.drawerAd;
    // },
    // getBannerAd: function (callback) {
    //     var self = this;
    //     if (self.adInfo.platAd.bannerAd == undefined) {
    //         self.adInfo.platAd.bannerAd = [];
    //     }
    //     return self.adInfo.platAd.bannerAd;
    // },
    // ClickAd: function (data, callback) {
    //     var self = this;
    //     console.log("ClickAd ClickAd ")
    //     if (data.wxAppId && data.miniQR == "") {
    //         self.navigateToAd(data, callback);
    //     } else if (data.miniQR) {
    //         self.previewImage(data, callback);
    //     }
    // },
    // navigateToAd: function (data, callback) {
    //     var self = this;
    //     if (data.wxAppId) {
    //         var sysInfo = wx.getSystemInfoSync()
    //         var pdata = {};
    //         pdata = sysInfo;
    //         pdata.adId = data.id
    //         pdata.gameId = self.gameId
    //         pdata.openId = self.userInfo.openId,
    //             wx.navigateToMiniProgram({
    //                 appId: data.wxAppId,
    //                 path: data.path,
    //                 extraData: data.extraData,
    //                 success: function () {
    //                     self.baseRequest({
    //                         url: "/adClick",
    //                         data: pdata,
    //                         success: function (res) {
    //                             var resInfo = res.data;
    //                             //console.log("res "+JSON.stringify(res))
    //                             if (resInfo.caId) {
    //                                 self._caId = resInfo.caId;
    //                             }
    //                             if (callback != undefined) {
    //                                 callback(res.data)
    //                             }
    //                         }
    //                     })
    //                 }
    //             });
    //     }
    // },
    // previewImage: function (data, callback) {
    //     var self = this;
    //     var sysInfo = wx.getSystemInfoSync()
    //     var pdata = {};
    //     pdata = sysInfo;
    //     pdata.adId = data.id
    //     pdata.gameId = self.gameId
    //     pdata.openId = self.userInfo.openId,
    //         wx.previewImage({
    //             urls: [data.miniQR],
    //             success: function success() {
    //                 self.baseRequest({
    //                     url: "/adClick",
    //                     data: pdata,
    //                     success: function success(res) {
    //                         var resInfo = res.data;
    //                         if (resInfo.caId) {
    //                             self._caId = resInfo.caId;
    //                         }
    //                         if (callback != undefined) {
    //                             callback(res.data)
    //                         }
    //                     }
    //                 });
    //             }
    //         });
    // },
    // pay: function (data, callback) {
    //     var self = this;
    //     self.baseRequest({
    //         url: "/createOrder",
    //         data: {
    //             gameId: self.gameId,
    //             openId: self.userInfo.openId,
    //             buyQuantity: data.buyQuantity,
    //             zone_id: data.zone_id,
    //             title: data.title
    //         },
    //         success: function (res) {
    //             console.log("res " + JSON.stringify(res))
    //             var info = res.data;
    //             wx.requestMidasPayment({
    //                 mode: "game",
    //                 env: self.gameInfo.env,
    //                 offerId: self.gameInfo.offer_id,
    //                 currencyType: "CNY",
    //                 platform: data.platform,
    //                 buyQuantity: data.buyQuantity,
    //                 zoneId: data.zone_id,
    //                 success: function (res) {
    //                     self.baseRequest({
    //                         url: "/payOrder",
    //                         data: {
    //                             gameId: self.gameId,
    //                             orderId: info.orderId,
    //                             buyQuantity: data.buyQuantity,
    //                             openId: self.userInfo.openId,
    //                         },
    //                         success: function (res0) {
    //                             console.log("payorder res0", JSON.stringify(res0))
    //                             if (callback != undefined) {
    //                                 callback(res0.data)
    //                             }
    //                         }
    //                     });
    //                 },
    //                 fail: function (res) {
    //                     console.log(res)
    //                 },
    //                 complete: function (res) {

    //                 },
    //             });
    //         }
    //     })
    // },
    onShareAppMessage(data) {
        var self = this;
        wx.onShareAppMessage({
            title: data.title,
            imageUrl: data.imageUrl,
            query: data.query
        });
    },
    shareAppMessage(data, callback) {
        var self = this;
        self._callback = callback;
        self.shareMessageFlag = true;
        if (data.sceneId == undefined || data.sceneId == null || data.sceneId == "") {
            data.sceneId = 0;
        }
        self.baseRequest({
            url: "/share",
            data: {
                sceneId: data.sceneId,
                gameId: self.gameId,
                openId: self.userInfo.openId,
            },
            success: function (res) {
                var querystr = data.query;
                if (querystr.indexOf("?") != -1) {
                    querystr = querystr + "&shareId=" + data.sceneId;
                } else {
                    querystr = querystr + "?shareId=" + data.sceneId;
                }
                wx.shareAppMessage({
                    title: data.title,
                    imageUrl: data.imageUrl,
                    query: querystr
                });
            }
        })
    },
    // updateRole(data, callback) {
    //     var self = this;
    //     self.baseRequest({
    //         url: "/updateRole",
    //         data: {
    //             roleName: data.roleName,
    //             serverId: data.serverId,
    //             gameId: self.gameId,
    //             openId: self.userInfo.openId,
    //             roleLevel: data.roleLevel,
    //             extInfo: data.extInfo
    //         },
    //         success: function (res) {
    //             if (callback != undefined) {
    //                 callback(res.data)
    //             }
    //         }
    //     })
    // },
    // getRole(data, callback) {
    //     var self = this;
    //     self.baseRequest({
    //         url: "/getRole",
    //         data: {
    //             serverId: data.serverId,
    //             gameId: self.gameId,
    //             openId: self.userInfo.openId
    //         },
    //         success: function (res) {
    //             if (callback != undefined) {
    //                 callback(res.data)
    //             }
    //         }
    //     })
    // },

    /**game api start */
    getUserInfo(callback) {
        var self = this;
        self.baseRequest({
            url: "/api/getUserInfo",

            success: function (res) {
                self.gameUserInfo = res.data.data;
                if (callback != undefined) {
                    callback(res.data)
                }
            }
        })
    },
    /**闯关选择关卡接口 */
    cgchoose(callback) {
        var self = this;
        self.baseRequest({
            url: "/api/cgchoose",

            success: function (res) {
                if (callback != undefined && res.data) {
                    callback(res.data);
                }
            }
        })
    },
    /**挑战模式首页 */
    challengeindex(callback) {
        var self = this;
        self.baseRequest({
            url: "/api/challengeindex",

            success: function (res) {
                if (callback != undefined && res.data) {
                    callback(res.data);
                }
            }
        })
    },
    /**闯关模式首页 */
    cgindex(game_id, callback) {
        var self = this;
        self.baseRequest({
            url: "/api/cgindex",
            data: {
                game_id: game_id
            },
            success: function (res) {
                if (callback != undefined && res.data) {
                    callback(res.data);
                }
            }
        })
    },
    /**我的卡券 */
    mycard(callback) {
        var self = this;
        self.baseRequest({
            url: "/api/mycard",
            data: {
                game_id: self.wxLoginCode
            },

            success: function (res) {
                if (callback != undefined && res.data) {
                    callback(res.data);
                }
            }
        })
    },
    /**玩游戏扣减体力 */
    deductpower(callback) {
        var self = this;
        self.baseRequest({
            url: "/api/deductpower",

            success: function (res) {
                if (callback != undefined && res.data) {
                    callback(res.data);
                }
            }
        })
    },
    /**助力好友 别人获得【体力值】 */
    getPower(share_uid, callback) {
        var self = this;
        self.baseRequest({
            url: "/api/getPower",
            data: {
                share_uid: share_uid
            },
            success: function (res) {
                if (callback != undefined && res.data) {
                    callback(res.data);
                }
            }
        })
    },
    /**助力好友 获得【小锤，炸弹】 */
    getProp(share_uid, type, callback) {
        var self = this;
        self.baseRequest({
            url: "/api/getProp",
            data: {
                share_uid: share_uid,
                type: type      //1锤子，2炸弹
            },

            success: function (res) {
                if (callback != undefined && res.data) {
                    if (res.data.type == 1) {           //加锤子
                    } else if (res.data.type == 2) {    //加炸弹
                    }
                    callback(res.data);
                }
            }
        })
    },
    /**每日任务 */
    everydayTask(callback) {
        var self = this;
        self.baseRequest({
            url: "/api/everydayTask",
            success: function (res) {
                if (callback != undefined && res.data) {
                    /**
                     * res.data = [
                     *  { 
                     *      task_id: 1, //任务id，用于领取
                     *      icon: 1,    //1.体力，2.炸弹，3.锤子，4.体力补给包
                     *      name: "每日签到得体力", 
                     *      reward: 1,  //icon上的x几
                     *      type: 1,    //1.可领取，2.邀请好友得体力，3.好友助力得体力，4.好友助力得炸弹，5.好友助力得锤子，6.已领取
                     *      curr: 0,    //当前完成进度
                     *      total: 1    //总进度
                     *  }
                     * ]
                     */
                    callback(res.data);
                }
            }
        })
    },
    /**领取每日任务奖励 */
    getTaskGift(task_id, callback) {
        var self = this;
        self.baseRequest({
            url: "/api/getTaskGift",
            data: {
                task_id: task_id
            },

            success: function (res) {
                if (callback != undefined && res.data) {
                    callback(res.data);
                }
            }
        })
    },
    /**保存闯关记录数据 */
    saveCgHistory(task_id, callback) {
        var data = {
            timestamp: (new Date()).getTime(),
            task_id: task_id,       //闯关id
        };

        data.sign = this.sign(data);
        this.baseRequest({
            url: "/api/saveCgHistory",
            data: data,

            success: function (res) {
                if (callback != undefined && res.data) {
                    callback(res.data);
                }
            }
        })
    },
    /**保存挑战模式分值 */
    saveTzHistory(callback) {
        var data = {
            timestamp: (new Date()).getTime(),
            score: Globals.infiniteScore,
        };
        data.sign = this.sign(data);
        this.baseRequest({
            url: "/api/saveTzHistory",
            data: data,

            success: function (res) {
                if (callback != undefined && res.data) {
                    callback(res.data);
                }
            }
        })
    },

    /**随机掉落奖券 */
    rankprize(callback) {
        const model = Globals.model, info = this.gameUserInfo;
        if (info.is_get) {
            return;
        }
        var data = {
            timestamp: (new Date()).getTime(),
            step: Globals.currStep,
            req_type: "cg"
        };
        var stepMin = 0, stepMax = 0;
        if (model == "scene") {
            stepMin = info.cg.step_min;
            stepMax = info.cg.step_max;
        } else {
            stepMin = info.tz.step_min;
            stepMax = info.tz.step_max;
            data.req_type = "tz";
        }
        if (Globals.currStep < stepMin || Globals.currStep > stepMax) {
            return;
        }
        data.sign = this.sign(data);
        this.baseRequest({
            url: "/api/rankprize",
            data: data,

            success: (res) => {
                if (callback != undefined && res.data) {
                    this.gameUserInfo.is_get = 1;
                    callback(res.data);
                }
            }
        })
    },
    /**本周排行 */
    weekrank(callback) {
        var self = this;
        self.baseRequest({
            url: "/api/weekrank",

            success: function (res) {
                if (callback != undefined && res.data) {
                    callback(res.data);
                }
            }
        })
    },
    /**本周排行 */
    lastweekrank(callback) {
        var self = this;
        self.baseRequest({
            url: "/api/lastweekrank",

            success: function (res) {
                if (callback != undefined && res.data) {
                    callback(res.data);
                }
            }
        })
    },
    /**领取上周排行奖励排行 */
    getWeekRankCard(callback) {
        var self = this;
        self.baseRequest({
            url: "/api/getWeekRankCard",

            success: function (res) {
                if (callback != undefined && res.data) {
                    callback(res.data);
                }
            }
        })
    },
    /**游戏界面补充体力 */
    invatelist(callback) {
        var self = this;
        self.baseRequest({
            url: "/api/invatelist",

            success: function (res) {
                if (callback != undefined && res.data) {
                    callback(res.data);
                }
            }
        })
    },
    /**我的奖券领取奖券 */
    getGiftCard(user_card_id, callback) {
        var self = this;
        self.baseRequest({
            url: "/api/getGiftCard",
            data: {
                user_card_id: user_card_id
            },

            success: function (res) {
                if (callback != undefined && res.data) {
                    callback(res.data);
                }
            }
        })
    },
    /**每日任务签到 */
    signIn(task_id, callback) {
        var self = this;
        self.baseRequest({
            url: "/api/signIn",
            data: {
                task_id: task_id
            },

            success: function (res) {
                if (callback != undefined && res.data) {
                    callback(res.data);
                }
            }
        })
    },
    /**使用道具 type => 1小锤，2炸弹*/
    deductProp(type, callback) {
        var self = this;
        self.baseRequest({
            url: "/api/deductProp",
            data: {
                type: type
            },

            success: function (res) {
                if (callback != undefined && res.data) {
                    callback(res.data);
                }
            }
        })
    },
    /**规则*/
    rule(callback) {
        var self = this;
        self.baseRequest({
            url: "/api/rule",

            success: function (res) {
                if (callback != undefined && res.data) {
                    callback(res.data);
                }
            }
        })
    },
    /**获取上周有没有排行榜奖励 */
    indexAlert(callback) {
        var self = this;
        self.baseRequest({
            url: "/api/indexAlert",

            success: function (res) {
                if (callback != undefined && res.data && res.data.data && res.data.data.gift_img) {
                    callback(res.data);
                }
            }
        })
    },
    /**game api end */
}
module.exports = JJ_MINIGAME_SDK