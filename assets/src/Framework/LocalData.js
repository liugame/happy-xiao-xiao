/**
 * 全局处理函数
 */
cc.Class({
    extends: cc.Component,

    properties: {
        _data: null,
    },

    //每次进入游戏进行判断 看是否是同一天
    initData: function () {
        let localData = cc.sys.localStorage.getItem("localData");
        if (localData == "" || localData == null || localData == undefined) {//判断是否有
            this._data = {
                bgPath: "images/bg_5/bg0",//背景路径
                atlasPath: "plist/oceanPlist",//图集路径
                props: [],
                letDoll: 0,//当前关卡
                door: 1,//可挑战关卡
                lastsigntime: 0,//记录时间
                //ownlist:"", 
                lasttime: Date.now(),//当前时间
                music: 1,//0为关闭，1为开启
                sound: 1,//0为关闭，1为开启
            }
        } else {
            this._data = JSON.parse(localData);//将字符转换为对象
            //cc.vv.qq.GetRankData(); 
        }
    },

    //获取数据
    getData: function (name) {
        if (this._data == null) {
            this.initData()
        }
        if (name == undefined) {
            return this._data;
        } else {
            return this._data[name];
        }
    },
    //修改数据
    setData: function (name, value) {
        if (this._data[name] !== null) {
            this._data[name] = value;
            cc.sys.localStorage.setItem("localData", JSON.stringify(this._data));
        }
    },
    //更新数据
    updateData: function (name, value) {
        if (this._data[name] !== null) {
            let cout = this._data[name] + value
            this._data[name] = cout;
            cc.sys.localStorage.setItem("localData", JSON.stringify(this._data));
        }
    },

    formatNickname: function (name) {
        if (name.length > 8) {
            return name.substring(0, 8) + "..";
        }
        return name;
    },

    /**
     * 获取originStr的key=variable的value
     * @param {string} variable 如 a得到1
     * @param {string} originStr 如 a=1&b=2&c=3
     */
    getQueryVariable(variable, originStr) {
        var varsParams = originStr.split("&");
        for (var i = 0; i < varsParams.length; i++) {
            var pair = varsParams[i].split("=");
            if (pair[0] == variable) {
                return pair[1];
            }
        }
        return "";
    },

    saveTzScore: function (callback) {
        if (Globals.infiniteScore != Globals.lastSaveScore) {
            cc.vv.JJSdk.saveTzHistory((data) => {
                if (!isNaN(data) && Globals.infiniteScore != data) {
                    cc.vv.netRootjs.dispatchEvent("netError", "上周分数已更新");
                    Globals.infiniteScore = data;
                }
                Globals.lastSaveScore = Globals.infiniteScore;
                callback && callback();
            });
        } else {
            callback && callback();
        }
    },

    /**是否可以助力
     * 返回值：0.不可助力，1.可助力，并且添加type，2.可助力，并且push
     */
    canHelpOther() {
        const query = Globals.shareQuery;
        if (query) {
            const type = query["type"], beHelpedId = query["beHelpedId"];
            if (beHelpedId == "" || beHelpedId == undefined || beHelpedId == cc.vv.JJSdk.JJ_MINIGAME_SDK.gameUserInfo.md5_id) {//没人帮助 || 不能给自己助力
                console.log(`是否可助力 0 => 被助力id: ${beHelpedId}, meId: ` + cc.vv.JJSdk.JJ_MINIGAME_SDK.gameUserInfo.md5_id);
                return 0;
            }
            let beHelpedIds = Globals.helpedData[type];     //该type下被帮助人的集合 
            if (!beHelpedIds || !beHelpedIds.length) {      //如果数组为空，就把这个塞进去，并且返回可助力  
                console.log(`是否可助力 1 => `, Globals.helpedData);
                return 1;
            }
            if (beHelpedIds.indexOf(beHelpedId) < 0) {     //如果助力数组找不到该id，说明没给这个人助力过，就把这个id塞到对应的助力数组，并且返回可助力
                console.log(`是否可助力 2 => `, Globals.helpedData);
                return 2;
            }
        }
        return 0;
    }
});
