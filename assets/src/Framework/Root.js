cc.Class({
    extends: cc.Component,

    properties: {
        bgAudio: {
            default: null,
            type: cc.AudioClip
        },
        maskNode: {
            default: null,
            type: cc.Node
        }
    },



    onLoad() {
        cc.vv = {};
        cc.vv.netRoot = this.node;//root节点
        cc.vv.netRootjs = this;//脚本

        this.maskNode.zIndex = 1000;
        this.hideMask();

        cc.game.addPersistRootNode(this.node);//赋予当前节点等级//常驻节点
        cc.game.addPersistRootNode(this.maskNode);//赋予当前节点等级//常驻节点

        //公用UI类
        var UIComment = require("UIComponent");
        cc.vv.UIComment = new UIComment();

        //数据交互类
        var LocalData = require("LocalData");
        cc.vv.LocalData = new LocalData();
        cc.vv.LocalData.initData();

        //公用音频类
        var MusicComment = require("AudioManager");
        cc.vv.MusicComment = new MusicComment();
        cc.vv.MusicComment.init(this.bgAudio);
        cc.vv.MusicComment.playBGM();

        var self = this;
        var JJSdk = require("./utils/JJSdk");
        cc.vv.JJSdk = new JJSdk();
        let data = {
            gameId: 13,
            version: "1.1.1",
            channelId: 9,
        }
        cc.vv.JJSdk.init(data, () => {
            //微信交互类
            var WXComment = require("WXComponent");
            cc.vv.WXComment = new WXComment();
            cc.vv.WXComment.init();
        });
        this.loadScene();

        if (typeof (wx) != "undefined") {
            wx.onHide(() => {
                cc.vv.MusicComment.onHide();
                cc.vv.LocalData.saveTzScore();
                cc.game.pause();
            });
            wx.onShow((res) => {
                Globals.shareQuery = res.query;
                console.log("wx.onShow ", Globals.shareQuery, res);
                cc.vv.MusicComment.onShow();
                this.helpDialog();
                if (cc.game.isPaused()) {
                    cc.game.resume();
                }
            });
        }
        else {
            cc.game.on(cc.game.EVENT_HIDE, () => {   //游戏进入后台后声音进行关闭 
                cc.vv.MusicComment.onHide();
                cc.vv.LocalData.saveTzScore();
            })
            cc.game.on(cc.game.EVENT_SHOW, () => {
                cc.vv.MusicComment.onShow();
                this.helpDialog();
            })
        }
    },

    start() {

    },

    loadScene: () => {
        //预加载场景资源 
        //    cc.director.preloadScene('GameMenu');
        cc.director.preloadScene('GameScene');
        cc.director.preloadScene('GameInfinite');
    },
    //发送事件（函数名，数据）
    dispatchEvent(event, data) {
        this.node.emit(event, data);
    },

    showMask() {
        this.maskNode.active = true;
    },
    hideMask() {
        this.maskNode.active = false;
    },

    /**
     * 获取是否需要给其他人助力，如果有上周排行榜奖励没领取，弹窗
     */
    helpDialog() {
        const canHelp = cc.vv.LocalData.canHelpOther();
        if (canHelp == 0) {
            //获取有没有上周奖励
            cc.vv.JJSdk.indexAlert(res=>{
                console.log("上周奖励：", res);
                cc.vv.netRootjs.dispatchEvent("weekRankReward", res);
            }) 
            return;
        }
        console.log("可助力 => ", canHelp);
        cc.vv.netRootjs.dispatchEvent("helpOther", canHelp);
    }

    // update (dt) {},
});
