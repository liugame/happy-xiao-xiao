/**
 * 普通模式的步数管理
 */
var DataMgr = require('DataMgr');
cc.Class({
    extends: cc.Component,

    properties: {
        limitLabel: {
            default: null,
            type: cc.Label
        },

        Prefab: {
            type: cc.Prefab,
            default: null
        },

    },

    onLoad() {
        this.init(Globals.door);
        var self = this;
        cc.vv.netRoot.on("doorNum", function () {
            self.doorNum();
        }, cc.vv.netRoot);
        cc.vv.netRoot.on("limitNum", function (data) {
            self.limitNum(data);
        }, cc.vv.netRoot);
    },
    start() {

    },
    onDestroy() {
        cc.vv.netRoot.off("doorNum");
        cc.vv.netRoot.off("limitNum");
    },
    init(id) {
        let data = DataMgr.doorDtMgr.getDataByID(id);
        this.id = data.id;
        this.img = data.img;
        this.num = data.num;
        this.limit = data.limit;
        this.reward = data.reward;
        Globals.doorLive = this.num;
        Globals.doorRewardUrl = this.reward;
        Globals.shieldChance = data.shieldChance;
        for (let i = 0; i < this.num; i++) {
            this.madeChlid(data.child[i]);
        }
        this.limitLabel.string = Globals.doorlimit + "";
        // this.doorLabel.string="第"+Globals.door+"关"; 
    },

    madeChlid: function (data) {
        var Prefab = cc.instantiate(this.Prefab);
        let PrefabCom = Prefab.getComponent('DoorChild');
        PrefabCom.init(data);
        this.node.addChild(Prefab); 
    },
    doorNum: function () {
        if (this.num == 0) {
            return;
        }
        this.num--;
        Globals.doorLive = this.num;
        if (this.num == 0) {
            Globals.winner = "winner";
        }
    },
    limitNum: function (count) {
        if (count) {
            Globals.doorlimit += count;
        }
        else {
            Globals.doorlimit--;
        }
        this.limitLabel.string = Globals.doorlimit.toString();
        if (Globals.doorlimit < 0 && this.num > 0) {
            Globals.winner = "loser";
        }
    },
});
