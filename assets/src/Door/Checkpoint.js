/**
 * 关卡选择单项
 */
var DataMgr = require('DataMgr');
cc.Class({
    extends: cc.Component,

    properties: {
        img: {
            default: null,
            type: cc.Node
        },
        id: 0,
        move: false,    //是否解锁
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        // this.init(this.id);
    },

    init: function (id, doorShowParent) {
        let data = DataMgr.doorDtMgr.getDataByID(id);
        this.id = id;
        this.species = data.species;
        this.mapID = data.mapID;
        this.doorShowParent = doorShowParent;

        this.move = this.id <= Globals.maxDoor + 1;
        this.setState();
    },
    setState: function () {
        const spriteCom = this.img.getComponent(cc.Sprite), atlas = 'plist/level';
        let img = 'lvOff' + this.id;
        if (this.move) {
            img = "lvOn" + this.id;
        }
        cc.vv.UIComment.loadSpriteFrame(img, spriteCom, atlas);
    },
    onClick: function () {
        cc.vv.MusicComment.playSFX("click");
        if (!this.move) {
            cc.vv.UIComment.showPrefab("hint", this.doorShowParent, "Hint", "请先解锁前面的关卡");
            return;
        }
        if (Globals.hp < Globals.cost) {
            cc.vv.UIComment.showPrefab("NoHpDialog", this.doorShowParent);
            return;
        }
        ///TODO:
        // Globals.hp -= Globals.cost;
        // let data = DataMgr.doorDtMgr.getDataByID(this.id);
        // Globals.doorLive = data.num;
        // Globals.door = this.id;
        // Globals.mapID = this.mapID;
        // Globals.elementNum = this.species;
        // Globals.model = "scene";
        // Globals.doorlimit = 2;
        // Globals.doorRwardUrl = "";
        // cc.director.loadScene("GameScene");
        // return;
        cc.vv.JJSdk.deductpower(() => {
            Globals.hp -= Globals.cost;
            let data = DataMgr.doorDtMgr.getDataByID(this.id);
            Globals.doorLive = data.num;
            Globals.door = this.id;
            Globals.mapID = this.mapID;
            Globals.elementNum = this.species;
            cc.vv.JJSdk.cgindex(this.id, res => {
                Globals.model = "scene";
                Globals.doorlimit = res.data.skip;
                Globals.doorRwardUrl = res.data.card_img;
                Globals.hp = res.data.power;
                Globals.malletNum = res.data.hammer_num;
                Globals.boomNum = res.data.bomb_num;
                cc.director.loadScene("GameScene");
            })
        });
    }
    // update (dt) {},
});
