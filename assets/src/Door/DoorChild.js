/**
 * 普通模式的闯关条件
 */
cc.Class({
    extends: cc.Component,

    properties: {
        imgN: {
            type: cc.Node,
            default: null
        },
        countLabel: {
            default: null,
            type: cc.Label
        },
        overN: {
            type: cc.Node,
            default: null
        },
    },


    onLoad() { },

    start() {
        var self = this;
        cc.vv.netRoot.on("doorCount", function (data) {
            if (self.type == data) {
                self.setCount();
            }
        }, cc.vv.netRoot);

    },
    onDestroy() {
        cc.vv.netRoot.off("doorCount");
    },
    init: function (data) {
        this.id = data.id;
        this.type = data.type;
        this.count = data.count;
        this.img = data.img;

        this.overN.active = false;
        var spriteCom = this.imgN.getComponent(cc.Sprite);

        let atlasPath = cc.vv.LocalData.getData("atlasPath");
        cc.vv.UIComment.loadSpriteFrame(this.img, spriteCom, atlasPath);

        if (Globals.doorLive % 2 == 0) {
            const center = Globals.doorLive / 2;
            this.node.x = (this.id - center) * this.node.width - this.node.width / 2;
        }
        else {
            const center = (Globals.doorLive + 1) / 2;
            this.node.x = (this.id - center) * this.node.width;
        }
        this.node.y = -25;

        this.countLabel.string = this.count.toString();
    },
    setCount: function () {
        if (this.count == 0) {
            return;
        }
        this.count--;
        this.countLabel.string = this.count.toString();
        if (this.count == 0) {
            this.overN.active = true;
            this.countLabel.node.active = false;
            cc.vv.netRootjs.dispatchEvent("doorNum");
        }
    }

    // update (dt) {},
});
