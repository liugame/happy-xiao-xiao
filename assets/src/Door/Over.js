/**
 * 结束界面
 */
var DataMgr = require('DataMgr');
cc.Class({
    extends: cc.Component,

    properties: {
        rewardImg: {
            default: null,
            type: cc.Node
        },
        titleImg: {
            default: null,
            type: cc.Node
        },
        nextBtn: {
            default: null,
            type: cc.Node
        },
        againBtn: {
            default: null,
            type: cc.Node
        },
        addLimitBtn: {
            default: null,
            type: cc.Node
        },
        txtImg: {
            default: null,
            type: cc.Node
        },
        titleSprteFrames: {
            type: cc.SpriteFrame,
            default: []
        },
        txtSprteFrames: {
            type: cc.SpriteFrame,
            default: []
        },
        labTip: {
            default: null,
            type: cc.Label
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        // cc.vv.NativeUtils.gameOver();
        if (Globals.winner == "winner") {
            this.winner();
        }
        else if (Globals.winner == "loser") {
            this.loser();
        }
        else {

        }
    },

    start() {
    },
    onDisable() {
        cc.vv.netRoot.off("double_Reward");
        cc.vv.netRoot.off("shareWin_activity");
    },
    loser: function () {
        cc.vv.MusicComment.playSFX("loser");

        this.titleImg.getComponent(cc.Sprite).spriteFrame = this.titleSprteFrames[1];
        this.txtImg.getComponent(cc.Sprite).spriteFrame = this.txtSprteFrames[1];
        this.againBtn.active = this.addLimitBtn.active = true;
        this.nextBtn.active = false;

        if (Globals.maxDoor >= Globals.door) {      //已经玩过本关
            // this.rewardImg.active = false;
        } else {
            const spriteCom = this.rewardImg.getComponent(cc.Sprite);
            cc.vv.UIComment.loadOriginImg(Globals.doorRwardUrl, spriteCom);
        }
    },
    winner: function () {
        cc.vv.MusicComment.playSFX("winner");

        let currNew = Globals.door > Globals.maxDoor;
        if (currNew) {   //说明闯新关成功
            cc.vv.JJSdk.saveCgHistory(Globals.door, res => {

            })
        }

        this.againBtn.active = this.addLimitBtn.active = false;
        this.nextBtn.active = true;

        let MaxDoor = DataMgr.doorDtMgr.getDataLeng();
        if (Globals.door == MaxDoor) {
            this.nextBtn.active = false;
        }

        if (currNew) {      //已经玩过本关
            this.rewardImg.active = this.txtImg.active = true;
            this.labTip.node.active = false;

            const spriteCom = this.rewardImg.getComponent(cc.Sprite);
            cc.vv.UIComment.loadOriginImg(Globals.doorRwardUrl, spriteCom);
            this.titleImg.getComponent(cc.Sprite).spriteFrame = this.titleSprteFrames[0];
            this.txtImg.getComponent(cc.Sprite).spriteFrame = this.txtSprteFrames[0];
        } else {
            this.rewardImg.active = this.txtImg.active = false;
            this.labTip.node.active = true;
        }

        Globals.maxDoor = Math.max(Globals.maxDoor, Globals.door);
    },
    next: function () {
        cc.vv.MusicComment.playSFX("click");
        const door = Globals.door + 1;
        if (door > Globals.maxDoor) {       //说明下一关没闯过
            if (Globals.hp < Globals.cost) {
                cc.vv.UIComment.showPrefab("NoHpDialog", this.node.getParent());
                return;
            }
        }
        cc.vv.JJSdk.deductpower(() => {
            Globals.hp -= Globals.cost;
            let data = DataMgr.doorDtMgr.getDataByID(door);
            Globals.doorLive = data.num;
            Globals.mapID = data.mapID;
            Globals.elementNum = data.species;
            cc.vv.JJSdk.cgindex(door, res => {
                Globals.door = door;
                Globals.doorlimit = res.data.skip;
                Globals.doorRwardUrl = res.data.card_img;
                Globals.hp = res.data.power;
                Globals.malletNum = res.data.hammer_num;
                Globals.boomNum = res.data.bomb_num;
                cc.director.loadScene("GameScene");
            })
        });
    },
    back: function () {
        cc.vv.MusicComment.playSFX("click");
        cc.director.loadScene("GameMenu");
    },
    again: function () {
        cc.vv.MusicComment.playSFX("click");
        if (Globals.winner == "infinite") {
            Globals.infiniteDoor = 1;
            Globals.doorlimit = 0;
            Globals.elementNum = 4;
            cc.director.loadScene("GameInfinite");
        }
        else {
            if (Globals.hp < Globals.cost) {
                cc.vv.UIComment.showPrefab("NoHpDialog", this.node.getParent());
                return;
            }
            cc.vv.JJSdk.deductpower(() => {
                Globals.hp -= Globals.cost;
                const id = Globals.door;
                cc.vv.JJSdk.cgindex(id, res => {
                    let data = DataMgr.doorDtMgr.getDataByID(id);
                    Globals.doorLive = data.num;
                    Globals.mapID = data.mapID;
                    Globals.elementNum = data.species;

                    Globals.doorlimit = res.data.skip;
                    Globals.doorRwardUrl = res.data.card_img;
                    Globals.hp = res.data.power;
                    Globals.malletNum = res.data.hammer_num;
                    Globals.boomNum = res.data.bomb_num;
                    cc.director.loadScene("GameScene");
                })
            });
        }
    },
    onAddLimitClick: function () {
        if (Globals.hp < Globals.addLimitNeedCostHp) {
            cc.vv.UIComment.showPrefab("NoHpDialog", this.node.getParent());
            return;
        }
        cc.vv.JJSdk.deductpower(() => {
            this.node.removeFromParent();
            Globals.hp -= Globals.addLimitNeedCostHp;
            cc.vv.netRootjs.dispatchEvent("addHp");
            cc.vv.netRootjs.dispatchEvent("addLimitNum");
            Globals.winner = "";
        });
    }
    // update (dt) {},
});
