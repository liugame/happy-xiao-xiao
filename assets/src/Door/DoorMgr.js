/**
 * 选关界面脚本
 */
var DataMgr = require('DataMgr');
cc.Class({
    extends: cc.Component,

    properties: {
        scrollView: {
            default: null,
            type: cc.ScrollView
        },
        labHp: {        //体力
            type: cc.Label,
            default: null
        },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        const maxDoor = DataMgr.doorDtMgr.getDataLeng();
        const children = this.scrollView.content.children;
        for (let i = 0; i < maxDoor; i++) {
            let item = children[i];
            item.getComponent("Checkpoint").init(i + 1, this.node); 
        }
    },

    start() { 
        this.labHp.string = Globals.hp + "/" + Globals.maxHp;
    },
    //关闭页面
    onCloseBtnClick: function () {
        cc.vv.MusicComment.playSFX("click");
        this.node.removeFromParent();
    },
    //增加体力点击
    onAddHpClick: function () {
        cc.vv.MusicComment.playSFX("click");
        cc.vv.JJSdk.invatelist(res => {
            cc.vv.UIComment.showPrefab("AddHpMrg", this.node, "AddHpMrg", res.data);
        })
    },

    // update (dt) {},
});
