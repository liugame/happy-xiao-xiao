/**
 * 元素处理
 */
var DataMgr = require('DataMgr');
cc.Class({
    extends: cc.Component,

    properties: {
        ShieldN: {
            type: cc.Node,
            default: null
        },
        StateLin: {
            type: cc.Node,
            default: null
        },
        Column: {
            type: cc.Node,
            default: null
        },
        ImgN: {
            type: cc.Node,
            default: null
        }
    },


    // onLoad () {},

    start() {

    },
    //初始化
    init: function (id, ShieldHp) {
        let data = DataMgr.elementDtMgr.getDataByID(id);
        this.id = data.id;
        this.score = Enum.elementScore;
        this.img = data.img;
        this.name = data.name;
        this.shieldHp = ShieldHp;


        this.vector = [];
        //加载图片
        let atlasPath = cc.vv.LocalData.getData("atlasPath");
        var spriteCom = this.ImgN.getComponent(cc.Sprite);
        cc.vv.UIComment.loadSpriteFrame(this.img, spriteCom, atlasPath);

        // spriteCom = this.StateLin.getComponent(cc.Sprite);
        // cc.vv.UIComment.loadSpriteFrame("state",spriteCom,atlasPath);
        // spriteCom = this.Column.getComponent(cc.Sprite);
        // cc.vv.UIComment.loadSpriteFrame("state",spriteCom,atlasPath);

        // this.StateLin.active = this.Column.active = false;
        this.hint(this.StateLin);
        this.hint(this.Column);

        this.node.zIndex = 2;
        this.state = data.state;
        this.changeShield();
        this.changeImg(this.state);
    },
    //提示 左右箭头 横竖消提示
    hint: function (node) {
        // let time = cc.delayTime(1.2); //延迟
        let action1 = cc.fadeTo(1, 0); //0.25秒透明度从255降到0
        let action2 = cc.fadeTo(1, 255); //0.25秒透明度从255降到0
        let seq = cc.sequence(action1, action2);
        let req = cc.repeatForever(seq)
        node.runAction(req);
    },
    //放大缩小提示（点击时的提示）
    selfHint: function () {
        // let time = cc.delayTime(1.2); //延迟
        this.node.stopAllActions();
        this.node.scale = cc.v2(1, 1)
        let action1 = cc.scaleTo(0.6, 0.8);
        let action2 = cc.scaleTo(0.6, 1);
        let seq = cc.sequence(action1, action2);
        let req = cc.repeatForever(seq)
        this.node.runAction(req);
    },
    selfHintOff: function () {
        this.node.stopAllActions();
        this.node.scale = cc.v2(1, 1)
    },
    //通过状态改变自己的样子 
    changeImg: function (state) {
        let atlasPath = cc.vv.LocalData.getData("atlasPath");
        switch (state) {
            case Enum.State.STATE_NOEN:
                this.StateLin.active = false;
                this.Column.active = false;
                break;
            case Enum.State.STATE_LINE:
                this.StateLin.active = true;
                this.Column.active = false;
                break;
            case Enum.State.STATE_COLUMN:
                this.StateLin.active = false;
                this.Column.active = true;
                break;
            case Enum.State.STATE_CROSS:
                this.StateLin.active = true;
                this.Column.active = true;
                break;
            case Enum.State.STATE_SIMILAR:
                this.StateLin.active = false;
                this.Column.active = false;

                var spriteCom = this.ImgN.getComponent(cc.Sprite);
                cc.vv.UIComment.loadSpriteFrame(this.name, spriteCom, atlasPath);
                break;

            case Enum.State.STATE_ANCHOR:
                this.StateLin.active = false;
                this.Column.active = false;

                var spriteCom = this.ImgN.getComponent(cc.Sprite);
                cc.vv.UIComment.loadSpriteFrame(this.name, spriteCom, atlasPath);
                break;
        }

    },
    //改变状态
    changeState: function (state) {
        if (this.state == state) {
            return;
        }
        this.state = state;
        if (this.state == Enum.State.STATE_SIMILAR) {
            this.name = Enum.State.STATE_SIMILAR;
        }
        else if (this.state == Enum.State.STATE_ANCHOR) {
            this.name = Enum.State.STATE_ANCHOR;
        }
        this.changeImg(this.state);
    },
    //改变护盾的样子
    changeShield: function () {
        if (this.shieldHp == 0) {
            this.ShieldN.active = false;
            return;
        }
        else {
            this.ShieldN.active = true;
            //加载图片
            let atlasPath = cc.vv.LocalData.getData("atlasPath");
            var spriteCom = this.ShieldN.getComponent(cc.Sprite);
            cc.vv.UIComment.loadSpriteFrame("shield", spriteCom, atlasPath);
        }
    },
    //改变护盾的血量
    setShieldHp: function (hp) {
        if (this.shieldHp == hp) {
            return;
        }
        this.shieldHp = hp;
        if (this.shieldHp == 0) {
            cc.vv.netRootjs.dispatchEvent("doorCount", "C02");
        }
        this.changeShield();
    },
    //制作移动动画
    madeAction: function (node) {
        this.vector.push(node);
        this.addAction(0);
    },

    //用递归实现 依次移动
    addAction: function (num) {
        if (num >= this.vector.length) {
            this.vector.length = 0;
            return;
        }
        let action1 = cc.moveTo(Enum.MoveTime / this.vector.length, cc.v2(this.vector[num].position));
        let action2 = cc.callFunc(function (target, next) {
            this.addAction(next);
        }, this, num + 1);
        let seq = cc.sequence(action1, action2);
        this.node.runAction(seq);
    }
    // update (dt) {},
});
