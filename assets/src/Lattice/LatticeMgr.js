﻿/**
 * 格子总管理类，消除逻辑
 */
var DataMgr = require('DataMgr');
cc.Class({
    extends: cc.Component,

    properties: {
        Prefab: {
            type: cc.Prefab,
            default: null
        },
        ElementPrefab: {
            type: cc.Prefab,
            default: null
        },
        LatticeN: {
            type: cc.Node,
            default: null
        },
        Move: false,
        MaskN: {
            type: cc.Node,
            default: null
        },
        hintN: {
            type: cc.Node,
            default: null
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        //提示动作（手指滑动）
        this.hintN.active = false;
        this.hintAction = function () {
            let node = this.arge.lattice;
            let state = this.arge.state;
            this.hintN.position = node.position;
            this.hintN.active = true;
            this.hintN.stopAllActions();

            let action1 = cc.callFunc(function () {
                this.hintN.position = node.position;
                this.hintN.opacity = 255;
            }, this);
            let action2;
            switch (state) {
                case "up":
                    action2 = cc.moveTo(2, cc.v2(node.x, node.y + this.Prefab.data.height));
                    break;
                case "down":
                    action2 = cc.moveTo(2, cc.v2(node.x, node.y - this.Prefab.data.height));
                    break;
                case "left":
                    action2 = cc.moveTo(2, cc.v2(node.x - this.Prefab.data.width, node.y));
                    break;
                case "right":
                    action2 = cc.moveTo(2, cc.v2(node.x + this.Prefab.data.width, node.y));
                    break;
            }
            let action3 = cc.fadeTo(0.1, 0); //0.25秒透明度从255降到0
            let seq = cc.sequence(action1, action2, action3);
            let req = cc.repeatForever(seq)
            this.hintN.runAction(req);
        }
    },

    start() {
        this.madeLattice();
        var self = this;
        //移动监听（元素操作后往哪边移动）
        cc.vv.netRoot.on("moveTo", function (agr) {
            let move = false;
            if (self.Move == false) {
                return;
            }
            let LatticeCom = agr.data.getComponent('Lattice');
            LatticeCom.hintOff();
            let letLatticeCom;
            if (LatticeCom.ElementN === null) {
                return;
            }
            if (self.LatticeN != null) {
                self.LatticeN.getComponent('Lattice').hintOff();
            }
            switch (agr.type) {
                case 0:
                    //不变
                    break;
                case Enum.NONE:
                    cc.vv.MusicComment.playSFX("click");
                    if (self.LatticeN == null) {
                        self.LatticeN = agr.data;
                        LatticeCom.hint();
                    }
                    else if (self.LatticeN == agr.data) {

                        LatticeCom.hint();
                    }
                    else {
                        LatticeCom = self.LatticeN.getComponent('Lattice');
                        self.LatticeN = agr.data;
                        letLatticeCom = self.LatticeN.getComponent('Lattice');
                        if ((letLatticeCom.latticeX - LatticeCom.latticeX == 0 && Math.abs(letLatticeCom.latticeY - LatticeCom.latticeY) == 1)
                            || (Math.abs(letLatticeCom.latticeX - LatticeCom.latticeX) == 1) && letLatticeCom.latticeY - LatticeCom.latticeY == 0) {

                            move = true;
                            self.hintOff();
                            letLatticeCom.hintOff();
                            LatticeCom.hintOff();
                            LatticeCom.changeElementN(self.LatticeN);

                        }
                        else {
                            letLatticeCom.hint();
                            LatticeCom.hintOff();
                        }
                    }
                    //不变
                    break;
                case Enum.LEFT:
                    //左
                    if (LatticeCom.latticeY > 0) {
                        self.LatticeN = self.lattice[LatticeCom.latticeX][LatticeCom.latticeY - 1];
                        letLatticeCom = self.LatticeN.getComponent('Lattice');
                        if (letLatticeCom.fixed == false) {
                            if (letLatticeCom.getState() == Enum.State.STATE_ANCHOR) {
                                self.LatticeN = null;
                                return;
                            }
                            move = true;
                            self.hintOff();
                            LatticeCom.changeElementN(self.lattice[LatticeCom.latticeX][LatticeCom.latticeY - 1])
                            //cc.vv.netRootjs.dispatchEvent("setlimit");
                        }
                        else {
                            self.LatticeN = null;
                        }
                    }

                    break;
                case Enum.RIGHT:
                    //右
                    if (LatticeCom.latticeY < self.latticeW - 1) {
                        self.LatticeN = self.lattice[LatticeCom.latticeX][LatticeCom.latticeY + 1]
                        letLatticeCom = self.LatticeN.getComponent('Lattice');
                        if (letLatticeCom.fixed == false) {
                            if (letLatticeCom.getState() == Enum.State.STATE_ANCHOR) {
                                self.LatticeN = null;
                                return;
                            }
                            move = true;
                            self.hintOff();
                            LatticeCom.changeElementN(self.lattice[LatticeCom.latticeX][LatticeCom.latticeY + 1])
                        }
                        else {
                            self.LatticeN = null;
                        }
                    }
                    break;
                case Enum.UP:
                    //上
                    if (LatticeCom.latticeX < self.latticeH - 1) {
                        self.LatticeN = self.lattice[LatticeCom.latticeX + 1][LatticeCom.latticeY]
                        letLatticeCom = self.LatticeN.getComponent('Lattice');
                        if (letLatticeCom.fixed == false) {
                            if (letLatticeCom.getState() == Enum.State.STATE_ANCHOR) {
                                self.LatticeN = null;
                                return;
                            }
                            move = true;
                            self.hintOff();
                            LatticeCom.changeElementN(self.lattice[LatticeCom.latticeX + 1][LatticeCom.latticeY])
                            //cc.vv.netRootjs.dispatchEvent("setlimit");
                        }
                        else {
                            self.LatticeN = null;
                        }
                    }
                    break;
                case Enum.DOWN:
                    //下
                    if (LatticeCom.latticeX > 0) {
                        self.LatticeN = self.lattice[LatticeCom.latticeX - 1][LatticeCom.latticeY]
                        letLatticeCom = self.LatticeN.getComponent('Lattice');
                        if (letLatticeCom.fixed == false) {
                            if (letLatticeCom.getState() == Enum.State.STATE_ANCHOR) {
                                self.LatticeN = null;
                                return;
                            }
                            move = true;
                            self.hintOff();
                            LatticeCom.changeElementN(self.lattice[LatticeCom.latticeX - 1][LatticeCom.latticeY])
                            //cc.vv.netRootjs.dispatchEvent("setlimit");
                        }
                        else {
                            self.LatticeN = null;
                        }
                    }
                    break;
            }
            if (self.LatticeN != null && move == true) {
                self.Move = false;
                self.MaskN.active = true;
                self.scheduleOnce(function () { self.moveToRemove(LatticeCom.node) }, Enum.EliminateTime);
            }
        }, cc.vv.netRoot);
        //状态消除监听（横竖消除之类的）
        cc.vv.netRoot.on("state", function (agr) {
            self.state(agr);
        }, cc.vv.netRoot);
        //刷新监听
        cc.vv.netRoot.on("refresh", function () {
            if (self.Move != false) {
                self.refresh();
                // cc.vv.UIComment.showPrefab("hint",self.node,"Hint","使用刷新道具");
            }
        }, cc.vv.netRoot);
        //锤子监听
        cc.vv.netRoot.on("mallet", function (arge) {
            if (self.Move != false) {
                self.mallet(arge);
            }
        }, cc.vv.netRoot);
        //炸弹监听
        cc.vv.netRoot.on("boom", function (arge) {
            if (self.Move != false) {
                self.boom(arge);
            }
        }, cc.vv.netRoot);
        //消除监听
        cc.vv.netRoot.on("repair", function () {
            self.removeElementN();
        }, cc.vv.netRoot);
        //结束界面加20步
        cc.vv.netRoot.on("addLimitNum", function () {
            self.openMove();
        }, cc.vv.netRoot);
    },
    onDestroy() {
        cc.vv.netRoot.off("moveTo");
        cc.vv.netRoot.off("state");
        cc.vv.netRoot.off("refresh");
        cc.vv.netRoot.off("mallet");
        cc.vv.netRoot.off("boom");
        cc.vv.netRoot.off("repair");
        cc.vv.netRoot.off("addLimitNum");
    },

    //----------------------创建---------------

    //创建矩阵
    madeLattice: function () {
        this.id = Globals.mapID;
        Globals.eliminateCount = 0;
        this.mapDt = DataMgr.mapDtMgr.getDataByID(this.id);
        this.latticeW = this.mapDt.mapY;
        this.latticeH = this.mapDt.mapX;
        this.lattice = [];
        this.vector = [];
        this.repairVector = [];
        for (var i = 0; i < this.latticeH; i++) {
            this.lattice[i] = [];
            for (var j = 0; j < this.latticeW; j++) {
                let position = cc.v2(0, 0);
                if (i === 0 && j === 0) {
                    position = cc.v2((-this.latticeW * this.Prefab.data.width / 2) + this.Prefab.data.width / 2 - 5, (-this.latticeH * this.Prefab.data.height / 2) + this.Prefab.data.height / 2 + 55);
                }
                else if (i === 0) {
                    position = cc.v2(this.lattice[i][j - 1].x + this.Prefab.data.width, this.lattice[i][j - 1].y);
                }
                else {
                    position = cc.v2(this.lattice[i - 1][j].x, this.lattice[i - 1][j].y + this.Prefab.data.height);
                }

                this.lattice[i][j] = this.madeChlid(this.mapDt.map[this.latticeH - i - 1][j], position, i, j);
            }
        }
        this.Move = true;
        this.MaskN.active = false;
        this.hintN.zIndex = 10;
        this.hint();
    },
    //创建格子
    madeChlid: function (Id, pos, x, y) {
        let id = Id;
        let prefab = cc.instantiate(this.Prefab);
        this.node.addChild(prefab);
        let latticeCom = prefab.getComponent('Lattice');
        prefab.position = pos;
        latticeCom.init(id, x, y);

        //不可见的就不创建了
        if (latticeCom.show == false) {
            latticeCom.node.opacity = 0;
            return prefab;
        }
        let Rondom;
        if (latticeCom.elementID != 0) {
            Rondom = latticeCom.elementID;
        }
        else {
            Rondom = Math.floor(Math.random() * Globals.elementNum) + 1;
            if (Rondom < 10) {
                Rondom = 'A0' + Rondom;
            }
            else {
                Rondom = 'A' + Rondom;
            }
            if (latticeCom.latticeX >= 2 && latticeCom.latticeY >= 2) {
                while (Rondom == this.lattice[latticeCom.latticeX - 2][latticeCom.latticeY].getComponent('Lattice').getId()
                    || Rondom == this.lattice[latticeCom.latticeX][latticeCom.latticeY - 2].getComponent('Lattice').getId()) {

                    Rondom = Math.floor(Math.random() * Globals.elementNum) + 1;
                    if (Rondom < 10) {
                        Rondom = 'A0' + Rondom;
                    }
                    else {
                        Rondom = 'A' + Rondom;
                    }
                }
            }
            else if (latticeCom.latticeY >= 2) {
                while (Rondom == this.lattice[latticeCom.latticeX][latticeCom.latticeY - 2].getComponent('Lattice').getId()) {
                    Rondom = Math.floor(Math.random() * Globals.elementNum) + 1;
                    if (Rondom < 10) {
                        Rondom = 'A0' + Rondom;
                    }
                    else {
                        Rondom = 'A' + Rondom;
                    }
                }
            }
            else if (latticeCom.latticeX >= 2) {
                while (Rondom == this.lattice[latticeCom.latticeX - 2][latticeCom.latticeY].getComponent('Lattice').getId()) {
                    Rondom = Math.floor(Math.random() * Globals.elementNum) + 1;
                    if (Rondom < 10) {
                        Rondom = 'A0' + Rondom;
                    }
                    else {
                        Rondom = 'A' + Rondom;
                    }
                }
            }
        }
        latticeCom.ElementN = this.madeElement(Rondom, pos, latticeCom.elementHp);
        return prefab;
    },
    //创建元素
    madeElement: function (id, pos, ShieldHp) {
        let prefab = cc.instantiate(this.ElementPrefab);
        let elementCom = prefab.getComponent('Element');
        prefab.position = pos;
        this.node.addChild(prefab);
        elementCom.init(id, ShieldHp);
        return prefab;
    },
    //替换矩阵
    changeLattice: function () {
        // let Rondom = Math.floor(Math.random() * Globals.mapInfinite.length);
        // let ID = Globals.mapInfinite[Rondom];
        // while (ID == Globals.mapID) {
        //     Rondom = Math.floor(Math.random() * Globals.mapInfinite.length);
        //     ID = Globals.mapInfinite[Rondom];
        // }
        // Globals.mapID = ID;
        ///TODO:难度控制
        if (Globals.elementNum < Globals.maxElementNum) {
            Globals.elementNum += 1;
        }
        // cc.director.loadScene("GameInfinite");
    },
    //是否消除
    isRemove: function (LatticeN) {
        let LatticeCom = LatticeN.getComponent('Lattice');
        if (LatticeCom.ElementN == null) {
            return false;
        }
        if (LatticeCom.eliminate == true || LatticeCom.getId() == Enum.State.STATE_ANCHOR) {
            return false;
        }
        if (LatticeCom.storage == true) {
            return true;
        }

        LatticeCom.reset();
        let vectorX = [];
        let vectorY = [];
        //向右遍历
        for (let i = LatticeCom.latticeY + 1; i < this.latticeW; i++) {
            let letLatticeCom = this.lattice[LatticeCom.latticeX][i].getComponent('Lattice');
            if (letLatticeCom.ElementN == null || letLatticeCom.eliminate == true) {
                break;
            }

            // let name=letLatticeCom.getId();
            if (LatticeCom.getId() == letLatticeCom.getId()) {
                LatticeCom.countX++;
                vectorX.push(this.lattice[LatticeCom.latticeX][i]);
            }
            else {
                break;
            }
        }
        //向左遍历
        for (let i = LatticeCom.latticeY - 1; i >= 0; i--) {
            let letLatticeCom = this.lattice[LatticeCom.latticeX][i].getComponent('Lattice');
            if (letLatticeCom.ElementN == null || letLatticeCom.eliminate == true) {
                break;
            }

            // let name=letLatticeCom.getId();
            if (LatticeCom.getId() == letLatticeCom.getId()) {
                LatticeCom.countX++;
                vectorX.push(this.lattice[LatticeCom.latticeX][i]);
            }
            else {
                break;
            }
        }
        //向上遍历
        for (let i = LatticeCom.latticeX + 1; i < this.latticeH; i++) {
            let letLatticeCom = this.lattice[i][LatticeCom.latticeY].getComponent('Lattice');
            if (letLatticeCom.ElementN == null || letLatticeCom.eliminate == true) {
                break;
            }

            // let name=letLatticeCom.getId();
            if (LatticeCom.getId() == letLatticeCom.getId()) {
                LatticeCom.countY++;
                vectorY.push(this.lattice[i][LatticeCom.latticeY]);
            }
            else {
                break;
            }
        }
        //向下遍历
        for (let i = LatticeCom.latticeX - 1; i >= 0; i--) {
            let letLatticeCom = this.lattice[i][LatticeCom.latticeY].getComponent('Lattice');
            if (letLatticeCom.ElementN == null || letLatticeCom.eliminate == true) {
                break;
            }

            // let name=letLatticeCom.getName();
            if (LatticeCom.getId() == letLatticeCom.getId()) {
                LatticeCom.countY++;
                vectorY.push(this.lattice[i][LatticeCom.latticeY]);
            }
            else {
                break;
            }
        }
        //判断
        if (LatticeCom.countY >= 3 || LatticeCom.countX >= 3) {
            this.vectorPush(LatticeN);
            if (LatticeCom.countX >= 3) {
                for (let i = 0; i < vectorX.length; i++) {
                    this.vectorPush(vectorX[i]);
                }
            }
            if (LatticeCom.countY >= 3) {
                for (let i = 0; i < vectorY.length; i++) {
                    this.vectorPush(vectorY[i]);
                }
            }
            return true;
        }
        return false;
    },
    //可移动
    openMove: function () {
        this.Move = true;
        this.MaskN.active = false;
        this.hint();
    },
    //移动后判断
    moveToRemove: function (LatticeN) {
        let LatticeCom1 = LatticeN.getComponent('Lattice');
        let LatticeCom2 = this.LatticeN.getComponent('Lattice');
        let bool1 = false;
        let bool2 = false;
        let bool3 = false;
        //消除同类的
        if (LatticeCom1.getState() == Enum.State.STATE_SIMILAR) {
            this.similar(this.LatticeN);
            this.vectorPush(LatticeN);
            this.vectorPush(this.LatticeN);
            bool3 = true;
        }
        else if (LatticeCom2.getState() == Enum.State.STATE_SIMILAR) {
            this.similar(LatticeN);
            this.vectorPush(LatticeN);
            this.vectorPush(this.LatticeN);
            bool3 = true;
        }
        //十字触发
        else if (LatticeCom1.getState() == Enum.State.STATE_CROSS) {
            if (LatticeCom2.getState() == Enum.State.STATE_LINE || LatticeCom2.getState() == Enum.State.STATE_COLUMN || LatticeCom2.getState() == Enum.State.STATE_CROSS) {
                this.vectorPush(LatticeN);
                this.vectorPush(this.LatticeN);
                bool3 = true;
            }
        }
        else if (LatticeCom2.getState() == Enum.State.STATE_CROSS) {
            if (LatticeCom1.getState() == Enum.State.STATE_LINE || LatticeCom1.getState() == Enum.State.STATE_COLUMN || LatticeCom1.getState() == Enum.State.STATE_CROSS) {
                this.vectorPush(LatticeN);
                this.vectorPush(this.LatticeN);
                bool3 = true;
            }
        }
        //组成十字
        else if (LatticeCom1.getState() == Enum.State.STATE_COLUMN || LatticeCom1.getState() == Enum.State.STATE_LINE) {
            if (LatticeCom2.getState() == Enum.State.STATE_COLUMN || LatticeCom2.getState() == Enum.State.STATE_LINE) {
                LatticeCom1.setState(Enum.State.STATE_NOEN);
                LatticeCom2.setState(Enum.State.STATE_CROSS);
                this.vectorPush(LatticeN);
                this.vectorPush(this.LatticeN);
                bool3 = true;
            }
        }

        if (bool3 == false) {
            bool1 = this.isRemove(LatticeN);
            bool2 = this.isRemove(this.LatticeN);
        }

        if (bool1 == true || bool2 == true || bool3 == true) {
            cc.vv.netRootjs.dispatchEvent("limitNum");
            Globals.currStep++;

            cc.vv.JJSdk.rankprize((res) => {
                cc.vv.netRoot.dispatchEvent("GiftFallDialog", res.data);
            })
            if (LatticeCom1.getState() == Enum.State.STATE_ANCHOR || LatticeCom2.getState() == Enum.State.STATE_ANCHOR) {
                cc.vv.netRootjs.dispatchEvent("doorCount", "C07");
            }
            this.LatticeN = null;

            this.removeElementN();
        }
        else {
            let LatticeCom = LatticeN.getComponent('Lattice');
            LatticeCom.changeElementN(this.LatticeN);
            LatticeCom.reset();

            this.LatticeN.getComponent('Lattice').reset();

            this.LatticeN = null;
            this.scheduleOnce(function () { this.openMove() }, Enum.MoveTime);
        }
    },
    //--------------------创建-over----------------

    //---------------------消除----------------------
    //消除-消除后修复
    removeElementN: function () {
        //消除
        cc.vv.MusicComment.playSFX("remove");
        if (Globals.model == "infinite") {
            cc.vv.netRootjs.dispatchEvent("score", this.vector.length);
        } else if (Globals.model == "scene") {
        }
        for (let i = 0; i < this.vector.length; i++) {
            let LatticeCom = this.vector[i].getComponent('Lattice');
            if (LatticeCom.ElementN !== null) {
                LatticeCom.removeElementN();
            }
        }
        this.vector.length = 0;

        //修复
        this.repairVectorPush();
        cc.vv.MusicComment.playSFX("repair");
        for (let i = 0; i < this.repairVector.length; i++) {
            this.repairElementN(this.repairVector[i]);
        }
        this.repairVector.length = 0;
        // let letTime=Enum.MoveTime+0.3;
        this.scheduleOnce(function () { this.allRemove() }, Enum.RepairTime);
    },

    //存储要消除的元素
    vectorPush: function (LatticeN) {
        let LatticeCom = LatticeN.getComponent('Lattice');
        if (LatticeCom.storage == true) {
            return;
        }
        // if(LatticeCom.storage==true||LatticeCom.eliminate==true){
        //     return;
        // }
        if (LatticeCom.ElementN != null) {
            if (LatticeCom.getState() == Enum.State.STATE_ANCHOR) {
                return;
            }
            this.vector.push(LatticeN);
            LatticeCom.storage = true;
        }
    },
    //修复后消除
    allRemove: function () {
        //this.move=false;
        // this.MaskN.active=true;
        let bool1 = false;
        for (let i = 0; i < this.latticeH; i++) {
            for (let j = 0; j < this.latticeW; j++) {
                let bool2 = this.isRemove(this.lattice[i][j]);
                if (bool2) {
                    bool1 = bool2;
                }
            }
        }
        bool1 = bool1;
        if (bool1) {
            Globals.eliminateCoun += 1;
            switch (Globals.eliminateCoun) {
                case 2:
                    cc.vv.MusicComment.playSFX("good");
                    break
                case 3:
                    cc.vv.MusicComment.playSFX("great");
                    break
                case 4:
                    cc.vv.MusicComment.playSFX("amazing");
                    break
                case 5:
                    cc.vv.MusicComment.playSFX("excellent");
                    break
            }
            this.removeElementN();
            return;
        }
        else {
            Globals.eliminateCoun = 0;
            if (Globals.model == "infinite") {
                if (Globals.infiniteScore >= Globals.infiniteDoor * Enum.infiniteScore) {
                    Globals.infiniteDoor++;
                    this.changeLattice();
                    this.Move = true;
                    this.MaskN.active = false;

                    return;
                }
                this.Move = true;
                this.MaskN.active = false;
                return;
            }
        }
        if (Globals.doorLive == 0 && Globals.doorlimit > 0) {
            cc.vv.MusicComment.playSFX("reward");
            // cc.vv.netRootjs.dispatchEvent("over");
            // this.callbackAAA = function(){
            //     this.limitRemove()
            // }
            // this.schedule(this.callbackAAA,0.1);
            this.scheduleOnce(function () { this.limitRemove() }, 0.1);
        }
        else if (Globals.doorLive == 0 && Globals.doorlimit <= 0) {
            Globals.winner = "winner";
            cc.vv.netRootjs.dispatchEvent("over");
        }
        else if (Globals.doorLive > 0 && Globals.doorlimit <= 0) {
            Globals.winner = "loser";
            cc.vv.netRootjs.dispatchEvent("over");
        }
        else {
            this.Move = true;
            this.MaskN.active = false;
            //提示
            this.hint();
        }
    },
    //步数消除
    limitRemove: function () {
        if (Globals.doorlimit <= 0) {
            // this.unschedule(this.callbackAAA);
            return;
        }
        let RondomX = Math.floor(Math.random() * this.latticeH);
        let RondomY = Math.floor(Math.random() * this.latticeW);
        let LatticeCom = this.lattice[RondomX][RondomY].getComponent('Lattice');
        while (LatticeCom.show == false || LatticeCom.fixed == true || LatticeCom.ElementN == null || LatticeCom.storage == true) {
            RondomX = Math.floor(Math.random() * this.latticeH);
            RondomY = Math.floor(Math.random() * this.latticeW);
            LatticeCom = this.lattice[RondomX][RondomY].getComponent('Lattice');
        }
        let Rondom = Math.floor(Math.random() * 3)
        switch (Rondom) {
            case 0:
                LatticeCom.setState(Enum.State.STATE_LINE);
                break;
            case 1:
                LatticeCom.setState(Enum.State.STATE_COLUMN);
                break;
            case 2:
                LatticeCom.setState(Enum.State.STATE_CROSS);
                break;
        }
        this.vectorPush(LatticeCom.node);
        cc.vv.netRootjs.dispatchEvent("limitNum");

        if (Globals.doorlimit <= 0) {
            // this.unschedule(this.callbackAAA);
            this.removeElementN();
        }
        else {
            this.scheduleOnce(function () { this.limitRemove() }, 0.1);
        }
    },
    //小木锤效果
    mallet: function (LatticeN) {
        if (this.Move == false) {
            return;
        }
        let LatticeCom = LatticeN.getComponent('Lattice');
        if (LatticeCom.getState() == Enum.State.STATE_ANCHOR) {
            return;
        }
        this.hintOff();
        this.Move = false;
        cc.vv.JJSdk.deductProp(1, res => {
            if (Globals.malletNum > 0) {
                cc.vv.UIComment.showPrefab("hint", this.node, "Hint", "使用成功");
                Globals.malletNum -= 1;
            }
            cc.vv.netRootjs.dispatchEvent("shutMallet", 1);
            cc.vv.netRootjs.dispatchEvent("addMalletNum");
            const clickX = LatticeCom.latticeX, clickY = LatticeCom.latticeY,
                poss = [
                    { x: clickX + 1, y: clickY },       //上一
                    { x: clickX, y: clickY - 1 },        //左一
                    { x: clickX, y: clickY },           //自身
                    { x: clickX, y: clickY + 1 },       //右一
                    { x: clickX - 1, y: clickY },       //下一
                ];
            for (let pos of poss) {
                if (pos.x < 0 || pos.x >= this.latticeH || pos.y < 0 || pos.y >= this.latticeW) continue;
                const latt = this.lattice[pos.x][pos.y];
                latt && this.vectorPush(latt);
            }
            this.removeElementN();
        });
    },
    //炸弹效果
    boom: function (LatticeN) {
        if (this.Move == false) {
            return;
        }
        let LatticeCom = LatticeN.getComponent('Lattice');
        if (LatticeCom.getState() == Enum.State.STATE_ANCHOR) {
            return;
        }
        this.hintOff();
        this.Move = false;
        cc.vv.JJSdk.deductProp(2, res => {
            Globals.boomNum -= 1;
            cc.vv.UIComment.showPrefab("hint", this.node, "Hint", "使用成功");
            cc.vv.netRootjs.dispatchEvent("shutMallet", 2);
            cc.vv.netRootjs.dispatchEvent("addBoomNum");

            this.cross(LatticeN);
            this.removeElementN();
        })
    },
    //-----------------------消除-over-----------------------------
    //-----------------------修复-----------------------------
    //查找要修复的元素
    repairVectorPush: function () {
        for (let i = 0; i < this.latticeH; i++) {
            for (let j = 0; j < this.latticeW; j++) {
                let letLatticeCom = this.lattice[i][j].getComponent('Lattice');
                if (letLatticeCom.ElementN == null && letLatticeCom.show == true) {
                    this.repairVector.push(this.lattice[i][j]);
                }
            }
        }
    },
    //障碍物判定
    obstaclesElementN: function (LatticeN) {
        let LatticeCom = LatticeN.getComponent('Lattice');
        if (LatticeCom.born == true) {
            return true;//创造点
        }
        if (LatticeCom.show == false || LatticeCom.eliminate == true) {
            return false;//空白或者石头冰块
        }

        let LatticeCom1 = this.lattice[LatticeCom.latticeX + 1][LatticeCom.latticeY].getComponent('Lattice');//普通的看上一个元素
        if (LatticeCom1.fixed == true) {
            return false;//石头或者冰块
        }
        else if (LatticeCom.born == true) {
            return true;//创造点
        }
        else {
            while (LatticeCom1.show == false && LatticeCom1.latticeX + 1 < this.latticeH) {  // 越过空白
                LatticeCom1 = this.lattice[LatticeCom1.latticeX + 1][LatticeCom1.latticeY].getComponent('Lattice');
            }
            if (LatticeCom1.fixed == true) {
                return false;
            }
            else {
                return true;
            }

        }
    },

    //修复单个元素
    repairElementN: function (LatticeN) {
        let LatticeCom = LatticeN.getComponent('Lattice');
        if (LatticeCom.ElementN != null) {
            return;
        }
        let j = LatticeCom.latticeY;
        for (let i = LatticeCom.latticeX + 1; i < this.latticeH; i++) {
            let LatticeCom1 = this.lattice[i - 1][j].getComponent('Lattice');
            let LatticeCom2 = this.lattice[i][j].getComponent('Lattice');
            //遇到空白越过
            let leti = i;
            while (LatticeCom2.show == false && LatticeCom2.born == false) {
                leti = leti + 1;
                LatticeCom2 = this.lattice[leti][j].getComponent('Lattice');
            }
            //遇到石头*****
            if (LatticeCom2.fixed == true && LatticeCom2.born == false) {
                let boolL;//左
                let boolR;//右
                if (j - 1 >= 0) {
                    boolL = this.obstaclesElementN(this.lattice[i - 1][j - 1]); //左边
                }
                else {
                    boolL = false;
                }
                if (j + 1 < this.latticeW) {
                    boolR = this.obstaclesElementN(this.lattice[i - 1][j + 1]);//右边
                }
                else {
                    boolR = false;
                }
                if (boolL == false && boolR == false) {
                    return;
                }
                else {
                    if (boolL == true) {
                        i = i - 1;
                        j = j - 1;
                    }
                    else {
                        i = i - 1;
                        j = j + 1;
                    }
                    LatticeCom2 = this.lattice[i][j].getComponent('Lattice');
                }
            }
            else {
                i = leti;
            }
            //遇到出生点
            if (LatticeCom2.born == true) {
                let Rondom = Math.floor(Math.random() * Globals.elementNum) + 1;
                if (Rondom < 10) {
                    Rondom = 'A0' + Rondom;
                }
                else {
                    Rondom = 'A' + Rondom;
                }
                let hp = Math.floor(Math.random() * 100) + 1;

                if (hp <= Globals.shieldChance) {
                    hp = 1;
                }
                else {
                    hp = 0;
                }
                LatticeCom2.ElementN = this.madeElement(Rondom, LatticeCom2.node.position, hp);
                LatticeCom1.changeElementN(LatticeCom2.node);
                return;
            }
            if (LatticeCom2.ElementN == null) {
                this.repairElementN(LatticeCom2.node);
            }
            LatticeCom1.changeElementN(LatticeCom2.node);
        }

    },
    //-----------------------修复-over-----------------------------
    //-----------------------------状态消除---------------------------
    //行消除
    line: function (Lattice) {
        let LatticeCom = Lattice.getComponent('Lattice');
        for (let i = 0; i < this.latticeW; i++) {
            this.vectorPush(this.lattice[LatticeCom.latticeX][i]);
        }
    },
    //列
    column: function (Lattice) {
        let LatticeCom = Lattice.getComponent('Lattice');
        for (let i = 0; i < this.latticeH; i++) {
            this.vectorPush(this.lattice[i][LatticeCom.latticeY]);
        }
    },
    //十字
    cross: function (Lattice) {
        this.line(Lattice);
        this.column(Lattice);
    },
    //同类
    similar: function (Lattice) {
        let LatticeCom = Lattice.getComponent('Lattice');
        cc.vv.MusicComment.playSFX("similar");
        for (let i = 0; i < this.latticeH; i++) {
            for (let j = 0; j < this.latticeW; j++) {
                let letLatticeCom = this.lattice[i][j].getComponent('Lattice');
                if (letLatticeCom.ElementN != null) {
                    if (letLatticeCom.getId() == LatticeCom.getId()) {
                        this.vectorPush(this.lattice[i][j]);
                    }
                }
            }
        }

    },
    //状态消除
    state: function (Lattice) {
        let LatticeCom = Lattice.getComponent('Lattice');
        switch (LatticeCom.getState()) {
            case Enum.State.STATE_LINE:
                cc.vv.MusicComment.playSFX("line");
                this.line(Lattice);
                break;
            case Enum.State.STATE_COLUMN:
                cc.vv.MusicComment.playSFX("line");
                this.column(Lattice);
                break;
            case Enum.State.STATE_CROSS:
                cc.vv.MusicComment.playSFX("line");
                this.cross(Lattice);
                break;
        }
    },
    //-----------------------------状态消除-over---------------------------
    //-----------------------刷新-----------------------------
    //刷新
    refresh: function () {
        if (this.Move == false) {
            return;
        }
        this.hintOff();
        this.Move = false;
        this.MaskN.active = true;
        for (let i = 0; i < this.latticeH; i++) {
            for (let j = 0; j < this.latticeW; j++) {
                let LatticeCom1 = this.lattice[i][j].getComponent('Lattice');
                if (LatticeCom1.show != false && LatticeCom1.fixed != true && LatticeCom1.ElementN != null && LatticeCom1.getState() != Enum.State.STATE_ANCHOR) {
                    let RondomX;
                    let RondomY;

                    let LatticeCom2;

                    do {
                        RondomX = Math.floor(Math.random() * this.latticeH);
                        RondomY = Math.floor(Math.random() * this.latticeW);
                        LatticeCom2 = this.lattice[RondomX][RondomY].getComponent('Lattice');
                    } while (LatticeCom2.show == false || LatticeCom2.ElementN == null || LatticeCom2.fixed == true || LatticeCom2.getState() == Enum.State.STATE_ANCHOR)
                    LatticeCom1.changeElementN(LatticeCom2.node);
                }
            }
        }
        this.scheduleOnce(function () { this.allRemove() }, Enum.RepairTime);
    },
    //-----------------------刷新-over----------------------------
    //-----------------------提示-----------------------------
    //判断是否可移动
    judgment: function (LatticeN, state) {
        let LatticeCom = LatticeN.getComponent('Lattice');
        if (LatticeCom.ElementN == null) {
            return false;
        }
        if (LatticeCom.show == false || LatticeCom.eliminate == true || LatticeCom.getId() == Enum.State.STATE_ANCHOR) {
            //空白或者冰块或者矛
            return false;
        }
        if (LatticeCom.fixed == true) {
            //石头
            return false;
            // if(state){
            //     //在上面
            //     return false;
            // }
        }
        if (LatticeCom.getState() == Enum.State.STATE_LINE || LatticeCom.getState() == Enum.State.STATE_COLUMN
            || LatticeCom.getState() == Enum.State.STATE_CROSS || LatticeCom.getState() == Enum.State.STATE_SIMILAR) {
            return LatticeCom.getState();
        }
        return true;
    },
    //判断是否提示
    judgmentHint: function (LatticeN, id) {
        let LatticeCom = LatticeN.getComponent('Lattice');
        if (LatticeCom.ElementN == null) {
            return false;
        }
        if (LatticeCom.show == false || LatticeCom.eliminate == true || LatticeCom.getId() == Enum.State.STATE_ANCHOR || LatticeCom.fixed == true) {
            //空白或者冰块或者矛
            return false;
        }
        if (LatticeCom.getId() == id) {
            return true;
        }
    },
    //判断自己
    judgmentSelf: function (LatticeN) {
        let LatticeCom = LatticeN.getComponent('Lattice');
        if (LatticeCom.ElementN == null) {
            return false;
        }
        if (LatticeCom.show == false || LatticeCom.eliminate == true || LatticeCom.getId() == Enum.State.STATE_ANCHOR || LatticeCom.fixed == true) {
            //空白或者冰块或者矛
            return false;
        }
        //向上
        if (LatticeCom.latticeX + 1 < this.latticeH - 1) {
            let up = this.judgment(this.lattice[LatticeCom.latticeX + 1][LatticeCom.latticeY], "up");
            if (up != false) {
                if (LatticeCom.getState() == Enum.State.STATE_LINE || LatticeCom.getState() == Enum.State.STATE_COLUMN
                    || LatticeCom.getState() == Enum.State.STATE_CROSS || LatticeCom.getState() == Enum.State.STATE_SIMILAR) {
                    if (up != true) {
                        return "up";
                    }
                }
                let countH = 1;
                let countW = 1;
                for (let i = LatticeCom.latticeX + 2; i < this.latticeH - 1; i++) {
                    if (this.judgmentHint(this.lattice[i][LatticeCom.latticeY], LatticeCom.getId()) == true) {
                        countH += 1;
                    }
                    else {
                        break;
                    }
                }
                for (let i = LatticeCom.latticeY + 1; i < this.latticeW; i++) {
                    if (this.judgmentHint(this.lattice[LatticeCom.latticeX + 1][i], LatticeCom.getId()) == true) {
                        countW += 1;
                    }
                    else {
                        break;
                    }
                }
                for (let i = LatticeCom.latticeY - 1; 0 <= i; i--) {
                    if (this.judgmentHint(this.lattice[LatticeCom.latticeX + 1][i], LatticeCom.getId()) == true) {
                        countW += 1;
                    }
                    else {
                        break;
                    }
                }
                if (countH >= 3 || countW >= 3) {
                    return "up";
                }
            }
        }
        //向下
        if (LatticeCom.fixed != true) {
            //不是石头
            if (0 <= LatticeCom.latticeX - 1) {
                let down = this.judgment(this.lattice[LatticeCom.latticeX - 1][LatticeCom.latticeY]);
                if (down != false) {
                    if (LatticeCom.getState() == Enum.State.STATE_LINE || LatticeCom.getState() == Enum.State.STATE_COLUMN
                        || LatticeCom.getState() == Enum.State.STATE_CROSS || LatticeCom.getState() == Enum.State.STATE_SIMILAR) {
                        if (down != true) {
                            return "down";
                        }
                    }
                    let countH = 1;
                    let countW = 1;
                    for (let i = LatticeCom.latticeX - 2; 0 <= i; i--) {
                        if (this.judgmentHint(this.lattice[i][LatticeCom.latticeY], LatticeCom.getId()) == true) {
                            countH += 1;
                        }
                        else {
                            break;
                        }
                    }
                    for (let i = LatticeCom.latticeY + 1; i < this.latticeW; i++) {
                        if (this.judgmentHint(this.lattice[LatticeCom.latticeX - 1][i], LatticeCom.getId()) == true) {
                            countW += 1;
                        }
                        else {
                            break;
                        }
                    }
                    for (let i = LatticeCom.latticeY - 1; 0 <= i; i--) {
                        if (this.judgmentHint(this.lattice[LatticeCom.latticeX - 1][i], LatticeCom.getId()) == true) {
                            countW += 1;
                        }
                        else {
                            break;
                        }
                    }
                    if (countH >= 3 || countW >= 3) {
                        return "down";
                    }
                }
            }
        }
        //向左
        if (0 <= LatticeCom.latticeY - 1) {
            let left = this.judgment(this.lattice[LatticeCom.latticeX][LatticeCom.latticeY - 1]);
            if (left != false) {
                if (LatticeCom.getState() == Enum.State.STATE_LINE || LatticeCom.getState() == Enum.State.STATE_COLUMN
                    || LatticeCom.getState() == Enum.State.STATE_CROSS || LatticeCom.getState() == Enum.State.STATE_SIMILAR) {
                    if (left != true) {
                        return "left";
                    }
                }

                let countH = 1;
                let countW = 1;
                for (let i = LatticeCom.latticeY - 2; 0 <= i; i--) {
                    if (this.judgmentHint(this.lattice[LatticeCom.latticeX][i], LatticeCom.getId()) == true) {
                        countW += 1;
                    }
                    else {
                        break;
                    }
                }
                for (let i = LatticeCom.latticeX + 1; i < this.latticeH - 1; i++) {
                    if (this.judgmentHint(this.lattice[i][LatticeCom.latticeY - 1], LatticeCom.getId()) == true) {
                        countH += 1;
                    }
                    else {
                        break;
                    }
                }
                for (let i = LatticeCom.latticeX - 1; 0 <= i; i--) {
                    if (this.judgmentHint(this.lattice[i][LatticeCom.latticeY - 1], LatticeCom.getId()) == true) {
                        countH += 1;
                    }
                    else {
                        break;
                    }
                }
                if (countH >= 3 || countW >= 3) {
                    return "left";
                }
            }
        }
        //向右  
        if (LatticeCom.latticeY + 1 < this.latticeW) {
            let right = this.judgment(this.lattice[LatticeCom.latticeX][LatticeCom.latticeY + 1]);
            if (right != false) {
                if (LatticeCom.getState() == Enum.State.STATE_LINE || LatticeCom.getState() == Enum.State.STATE_COLUMN
                    || LatticeCom.getState() == Enum.State.STATE_CROSS || LatticeCom.getState() == Enum.State.STATE_SIMILAR) {
                    if (right != true) {
                        return "right";
                    }
                }
                let countH = 1;
                let countW = 1;
                for (let i = LatticeCom.latticeY + 2; i < this.latticeW; i++) {
                    if (this.judgmentHint(this.lattice[LatticeCom.latticeX][i], LatticeCom.getId()) == true) {
                        countW += 1;
                    }
                    else {
                        break;
                    }
                }
                for (let i = LatticeCom.latticeX + 1; i < this.latticeH - 1; i++) {
                    if (this.judgmentHint(this.lattice[i][LatticeCom.latticeY + 1], LatticeCom.getId()) == true) {
                        countH += 1;
                    }
                    else {
                        break;
                    }
                }
                for (let i = LatticeCom.latticeX - 1; 0 <= i; i--) {
                    if (this.judgmentHint(this.lattice[i][LatticeCom.latticeY + 1], LatticeCom.getId()) == true) {
                        countH += 1;
                    }
                    else {
                        break;
                    }
                }
                if (countH >= 3 || countW >= 3) {
                    return "right";
                }
            }
        }
        return false;

    },
    //关闭提示
    hintOff: function () {
        this.unschedule(this.hintAction);
        this.hintN.active = false;
    },
    //提示
    hint: function () {
        let bool;
        for (let i = 0; i < this.latticeH; i++) {
            for (let j = 0; j < this.latticeW; j++) {
                bool = this.judgmentSelf(this.lattice[i][j]);
                if (bool != false) {
                    this.arge = { lattice: this.lattice[i][j], state: bool }

                    this.scheduleOnce(this.hintAction, 5);
                    return this.arge;
                }
            }
        }
        this.refresh();
        return false

    },

    //-----------------------提示-over----------------------------
    // update (dt) {},
});
